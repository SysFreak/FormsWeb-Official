<?php

namespace App\Functions;

use Illuminate\Http\Request;

class Helpers
{
    public static function GetRequest($request)
    {
        $tokenType      =   substr($request->header('Authorization'), 0,6);
        $token          =   substr($request->header('Authorization'), 7, strlen($request->header('Authorization')));
        $contentType    =   $request->header('Content-Type');
        $body           =   $request->input();

        if($contentType == "application/x-www-form-urlencoded")
        {
            return [
                'status'    =>  true,
                'body'      =>  $body,
            ];
        }else{
            return [
                'status'    =>  false,
                'message'   =>  "Content Type NO Supported"
            ];
        }

        return [
            "token-type"    =>  $tokenType,
            'token'         =>  $token,
            'content-type'  =>  $contentType,
            'body'          =>  $body,
        ];
    }

    public static function SingleStatusTrue($data)
    {
        $params     =   [];
        parse_str($data, $params);

        return $params;

        $iData  =   [];
        foreach($data as $k => $val)
        {
            if($val->status == 1)
            {
                $iData[$k]  =   [
                    'uuid'  =>  $val->uuid,
                    'name'  =>  $val->name
                ];
            }
        }

        if($iData)
        {
            return [
                'status'    =>  true,
                'data'      =>  $iData
            ];
        }else{
            return [
                'status'    =>  false
            ];
        }

    }

    public static function SingleStatusApi($data)
    {
        $params     =   [];
        parse_str($data, $params);

        $iData  =   [];
        foreach($params as $k => $val)
        {
            var_dump($k, $val);
            if(($k == 'status_id') && ($val == 1))
            {
                var_dump($k, $val);exit;
                $iData[$k]  =   [
                    'uuid'  =>  $val['uuid'],
                    'name'  =>  $val['name']
                ];
            }
        }

        if($iData)
        {
            return [
                'status'    =>  true,
                'data'      =>  $iData
            ];
        }else{
            return [
                'status'    =>  false
            ];
        }

    }

    public static function ComplexStatusTrue($data)
    {
        $iData  =   $iD =   [];

        if($data)
        {
            foreach($data as $k => $val)
            {
                $cont   =   0;
                foreach($val->fillable as $ff => $fl){ $cont++; }

                for($i = 0; $i < $cont; $i++)
                {
                    $field[$i] =   $val->fillable[$i];
                    $values[$i] =   $val[$val->fillable[$i]];
                }

                $iD[$k]  = array_combine($field, $values);

                if($iD[$k]['status_id'] == 1)
                {
                    $iData[$k]      =   $iD[$k];
                }
            }

            if($iData)
            {
                return [
                    'status'    =>  true,
                    'data'      =>  $iData
                ];
            }else{
                return [
                    'status'    =>  false
                ];
            }

        }else{
            return [
                'status'    =>  false
            ];
        }


    }

    public static function InfoClient($data)
    {

        return [
			'TITLE'                 =>  'Formulario Web - '. ucfirst($data->Nombre).' '.ucfirst($data->Apellido),
            'NAME'                  =>  ucfirst($data->Nombre),
            'LAST_NAME'             =>  ucfirst($data->Apellido),
            'ADDRESS'               =>  ucfirst($data->Urbanizacion),
            'UF_CRM_1633014687'     =>  ucfirst($data->Urbanizacion),
            'BIRTHDATE'				=>	$data->Nacimiento,
            'UF_CRM_1619017239'     =>  4953,
            'UF_CRM_1609797241764'	=>	ucfirst($data->Cedula),
            'PHONE'     =>  [
                [ 'VALUE' => ($data->Principal)   ? '+58'.$data->Principal  : '',	'VALUE_TYPE' => 'WORK' ],
                [ 'VALUE' => ($data->Alterno)     ? '+58'.$data->Alterno    : '',	'VALUE_TYPE' => 'WORK' ]
            ],
            'EMAIL'     =>  [
                [ 'VALUE' => ($data->Email)       ? strtolower($data->Email) : '',  'VALUE_TYPE' => 'WORK' ]
            ],
            'UF_CRM_1609794549759'  =>  55,
            'UF_CRM_1612122471641'  =>  2427,
            'SOURCE_ID'             =>  19,
            'UF_CRM_1609795291409'  =>  71,
            'UF_CRM_1609795327159'  =>  Helpers::InfoRoof($data->Techo),
            'UF_CRM_1609795433477'  =>  Helpers::InfoLevel($data->Niveles),
            'UF_CRM_1609794790022'  =>  69,
            'UF_CRM_1609795771'     =>  Helpers::InfoCountry(ucfirst($data->Estado)),
            'UF_CRM_1609795211284'  =>  $data->Latitud,
            'UF_CRM_1609795234849'  =>  $data->Longitud,
            'COMMENTS'				=>	''
		];
    }

    public static function InfoMassiveClient($data)
    {

        return [
			'TITLE'                 =>  ' '. ucfirst($data->name).' '.ucfirst($data->last_name. ' // No Señal'),
            'NAME'                  =>  ucfirst($data->name),
            'LAST_NAME'             =>  ucfirst($data->last_name),
            'ADDRESS'               =>  ucfirst($data->address),
            'UF_CRM_1633014687'     =>  ucfirst($data->address),
            'BIRTHDATE'				=>	'',
            'UF_CRM_1619017239'     =>  4953,
            'UF_CRM_1609797241764'	=>	'',
            'PHONE'     =>  [
                [ 'VALUE' => ($data->phone)         ? '+58'.$data->phone            : '',	'VALUE_TYPE' => 'WORK' ],
                [ 'VALUE' => ($data->alternative)   ? '+58'.$data->alternative      : '',	'VALUE_TYPE' => 'WORK' ]
            ],
            'EMAIL'     =>  [
                [ 'VALUE' => ($data->email)       ? strtolower($data->email) : '',  'VALUE_TYPE' => 'WORK' ]
            ],
            'UF_CRM_1609794549759'  =>  55,
            'UF_CRM_1612122471641'  =>  2427,
            'SOURCE_ID'             =>  19,
            'UF_CRM_1609795291409'  =>  71,
            'UF_CRM_1609795327159'  =>  '',
            'UF_CRM_1609795433477'  =>  '',
            'UF_CRM_1609794790022'  =>  69,
            'UF_CRM_1609795771'     =>  '',
            'UF_CRM_1609795211284'  =>  '',
            'UF_CRM_1609795234849'  =>  '',
            'COMMENTS'				=>	'CARGA MASIVA DE CLIENTES NO SEñAL'
		];
    }

    public static function InfoCountry($data)
    {
        switch ($data)
        {
            case 'Amazonas':            return "197"; break;
            case 'Anzoategui':          return "199"; break;
            case 'Apure':               return "201"; break;
            case 'Aragua':              return "203"; break;
            case 'Barinas':             return "205"; break;
            case 'Bolivar':             return "207"; break;
            case 'Carabobo':            return "209"; break;
            case 'Cojedes':             return "211"; break;
            case 'Delta Amacuro':       return "213"; break;
            case 'Distrito Capital':    return "215"; break;
            case 'Falcon':              return "217"; break;
            case 'Guarico':             return "219"; break;
            case 'Lara':                return "221"; break;
            case 'Merida':              return "223"; break;
            case 'Miranda':             return "225"; break;
            case 'Monagas':             return "227"; break;
            case 'Nueva Esparta':       return "229"; break;
            case 'Portuguesa':          return "231"; break;
            case 'Sucre':               return "233"; break;
            case 'Tachira':             return "235"; break;
            case 'Trujillo':            return "237"; break;
            case 'Vargas':              return "239"; break;
            case 'Yaracuy':             return "241"; break;
            case 'Zulia':               return "243"; break;
            default:                    return "221"; break;
        }
    }

    public static function InfoRoof($data)
    {
        switch ($data)
        {
            case 'Cemento':     return "81";    break;
            case 'Zinc':        return "85";    break;
            case 'Madera':      return "83";    break;
            case 'Machiembrado':return "5081";  break;
            case 'Acerolit':    return "5083";  break;
            case 'Mixto':       return "5085";  break;
            default:            return "81";    break;
        }
    }

    public static function InfoLevel($data)
    {
        switch ($data)
        {
            case 'Un Nivel':            return "87"; break;
            case 'Dos Niveles':         return "89"; break;
            case 'Tres Niveles':        return "91"; break;
            case 'Cuatro Niveles':      return "93"; break;
            case 'Edificio':            return "95"; break;
            default:                    return "87"; break;
        }
    }

}
