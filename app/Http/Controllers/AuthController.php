<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Functions\Helpers;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Models\FormsName;

class AuthController extends Controller
{
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validate  = [ 'u'  => (isset($request->username)), 'p'  => (isset($request->password)), 'f' => (isset($request->form)) ];

        if( ($validate['u']) && ($validate['p']) && ($validate['f']) )
        {
            $data   =   Helpers::GetRequest($request);
            $form   =   FormsName::GetFormById($data['body']['form']);

            if($form)
            {
                $credentials    =   $request->only('username', 'password');


                try {

                    if(!$token  =   JWTAuth::attempt($credentials))
                    {
                        return response()->json(['error' => 'Invalid Credentials'], 401);
                    }

                } catch (JWTException $e) {

                    return response()->json(['error' => 'Could not create token'], 405);

                }

                return response()->json(['status' => 'success', 'token' => $token, 'expires_in' => auth()->factory()->getTTL() * 60 ], 200);

            }else{
                return response()->json(['error' => 'Dont have authorization'], 400);
            }

            return response()->json($request, 200);

        }else{

            return response()->json(['error' => 'Invalid Credentials'], 401);

        }

    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user   = auth('api')->user();

        return  response()->json(['user' => $user], 201);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ], 200);
    }
}
