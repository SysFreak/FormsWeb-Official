<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;

use App\Http\Controllers\Controller;
use App\Functions\Helpers;

use App\Models\User;
use App\Models\FormsName;
use App\Models\FormsType;
use App\Models\FormsEmails;
use App\Models\FormsUsers;
use App\Models\DataStatus;

use Hash;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class PanelController extends Controller
{
    public function view(Request $request)
    {
        return view('panel.login.index');
    }

    public function dashboard(Request $request)
    {
        $validation  =   Validator::make($request->all(), [
            'username'  => 'required|min:5|max:50',
            'password'  => 'required|min:5|max:50',
        ]);

        if ($validation->fails())
        {
            return \Redirect::back()->withErrors($validation)->withInput();
        }

        $token   =   Auth::attempt(['username' => $request->username, 'password' => $request->password]);

        if($token)
        {
            $user   =   User::where('username','=',$request->username)->select('name','id', 'username')->first();

            session(['username' => $user->username, 'name' => $user->name, 'id' => $user->id]);

            return view('panel.dashboard.index',[
                'token'     => $token,
                'username'  => $user->name
            ]);
        }else{
            return \Redirect::back()->withErrors('Invalid Credentials')->withInput();
        }


    }

    public function forms(Request $request)
    {
        $heplers      =   Helpers::GetRequest($request);

        if($heplers['status'] == false)
        {
            return \response()->json([
                'status'    =>  false,
                'error'   =>  $heplers['message'],
            ], 401);
        }else{

            return \response()->json([
                'status'    =>  true,
                'username'  =>  $request->username,
                'token'     =>  $request->token,
                'fname'     =>  FormsName::GetForms(),
                'ftype'     =>  FormsType::GetFormsTable(),
                'fusers'    =>  FormsUsers::GetFormsTable(),
                'femail'    =>  FormsEmails::GetFormsTable(),
                'status'    =>  DataStatus::GetAll(),
                'formsTab'  =>  FormsName::GetFormsTable(),
            ], 200);
        }
    }

    public function formsEdit(Request $request)
    {
        $heplers      =   Helpers::GetRequest($request);

        if($heplers['status'] == false)
        {
            return \response()->json([
                'status'    =>  false,
                'error'   =>  $heplers['message'],
            ], 401);
        }else{

            return \response()->json([
                'status'    =>  true,
                'token'     =>  $request->token,
                'forms'     =>  FormsName::GetUsersFormsId($heplers['body']['id'])
            ], 200);
        }
    }

    public function formsRegister(Request $request)
    {
        $heplers      =   Helpers::GetRequest($request);

        if($heplers['status'] == false)
        {
            return \response()->json([
                'status'    =>  false,
                'error'   =>  $heplers['message'],
            ], 401);
        }else{

            parse_str($heplers['body']['data'], $params);

            if($params['name_forms'] == '')
            {
                return \response()->json([
                    'status'    =>  false,
                    'token'     =>  $request->token,
                    'message'   =>  'Debe introducir un nombre valido'
                ], 200);
            }

            $pass   =   Str::random(40);
            $_new    =   new FormsName();
            $_new->uuid         =   \Ramsey\Uuid\Uuid::uuid4()->toString();
            $_new->name         =   strtolower($params['name_forms']);
            $_new->password     =   $pass;
            $_new->type_id      =   $params['type_forms'];
            $_new->status_id    =   1;
            $info               =   $_new->save();

            $_user  =   new User();
            $_user->uuid        =   \Ramsey\Uuid\Uuid::uuid4()->toString();
            $_user->name        =   strtolower($params['name_forms']);
            $_user->username    =   strtolower($params['name_forms']);
            $_user->email       =   strtolower($params['name_forms']).'@boomsolutions.com';
            $_user->password    =   bcrypt($pass);
            $_user->save();


            return \response()->json([
                'status'    =>  true,
                'token'     =>  $request->token,
            ], 200);
        }
    }

    public function formsUpdate(Request $request)
    {
        $heplers      =   Helpers::GetRequest($request);

        if($heplers['status'] == false)
        {
            return \response()->json([
                'status'    =>  false,
                'error'   =>  $heplers['message'],
            ], 401);
        }else{

            parse_str($heplers['body']['data'], $params);

            if($params['name_forms_edits'] == '')
            {
                return \response()->json([
                    'status'    =>  false,
                    'token'     =>  $request->token,
                    'message'   =>  'Debe introducir un nombre valido'
                ], 200);
            }
            // var_dump($params);exit;
            $_edit              =   FormsName::GetFormID($params['id_forms_edits']);
            $_edit->name        =   strtolower($params['name_forms_edits']);
            $_edit->type_id     =   $params['type_forms_edits'];
            $_edit->status_id   =   DataStatus::GetById($params['status_forms_edits'])[0]->number;
            $_edit->save();

            return \response()->json([
                'status'    =>  true,
                'token'     =>  $request->token,
            ], 200);
        }
    }

    public function formsTypesEdit(Request $request)
    {
        $heplers      =   Helpers::GetRequest($request);

        if($heplers['status'] == false)
        {
            return \response()->json([
                'status'    =>  false,
                'error'   =>  $heplers['message'],
            ], 401);
        }else{

            return \response()->json([
                'status'    =>  true,
                'token'     =>  $request->token,
                'forms'     =>  FormsType::GetFormsById($heplers['body']['id'])
            ], 200);
        }
    }

    public function formsTypesRegister(Request $request)
    {
        $heplers      =   Helpers::GetRequest($request);

        if($heplers['status'] == false)
        {
            return \response()->json([
                'status'    =>  false,
                'error'   =>  $heplers['message'],
            ], 401);
        }else{

            parse_str($heplers['body']['data'], $params);

            if($params['name_types'] == '')
            {
                return \response()->json([
                    'status'    =>  false,
                    'token'     =>  $request->token,
                    'message'   =>  'Debe introducir un nombre valido'
                ], 200);
            }

            $_new   =   new FormsType;
            $_new->uuid         =   \Ramsey\Uuid\Uuid::uuid4()->toString();
            $_new->name         =   ucfirst($params['name_types']);
            $_new->users_id     =   $params['op_types'];
            $_new->status_id    =   1;
            $_new->save();

            return \response()->json([
                'status'    =>  true,
                'token'     =>  $request->token,
            ], 200);
        }
    }

    public function formsTypesUpdate(Request $request)
    {
        $heplers      =   Helpers::GetRequest($request);

        if($heplers['status'] == false)
        {
            return \response()->json([
                'status'    =>  false,
                'error'   =>  $heplers['message'],
            ], 401);
        }else{

            parse_str($heplers['body']['data'], $params);


            if($params['name_types_edits'] == '')
            {
                return \response()->json([
                    'status'    =>  false,
                    'token'     =>  $request->token,
                    'message'   =>  'Debe introducir un nombre valido'
                ], 200);
            }

            $_edit              =   FormsType::GetFormById($params['id_types_edits']);
            $_edit->name        =   ucfirst($params['name_types_edits']);
            $_edit->users_id    =   $params['op_types_edits'];
            $_edit->status_id   =   DataStatus::GetById($params['status_types_edits'])[0]->number;
            $_edit->save();

            return \response()->json([
                'status'    =>  true,
                'token'     =>  $request->token,
            ], 200);
        }
    }

    public function types(Request $request)
    {
        $heplers      =   Helpers::GetRequest($request);
        if($heplers['status'] == false)
        {
            return \response()->json([
                'status'    =>  false,
                'error'   =>  $heplers['message'],
            ], 401);
        }else{
            $type   =   new FormsType;
            $type->name     =   ucfirst($heplers['body']['name']);
            $type->save();

            if($type)
            {
                return \response()->json([
                    'status'    =>  true,
                    'data'      =>  FormsType::GetForms(),
                ], 201);
            }else{
                return \response()->json([
                    'status'    =>  true,
                    'error'     =>  "Error en creacion de Usuario",
                ], 400);
            }

        }
        dd($heplers);
    }

    public function formsUsersEdit(Request $request)
    {
        $heplers      =   Helpers::GetRequest($request);

        if($heplers['status'] == false)
        {
            return \response()->json([
                'status'    =>  false,
                'error'   =>  $heplers['message'],
            ], 401);
        }else{


            return \response()->json([
                'status'    =>  true,
                'token'     =>  $request->token,
                'forms'     =>  FormsUsers::GetFormById($heplers['body']['id'])
            ], 200);
        }
    }

    public function formsUsersUpdate(Request $request)
    {
        $heplers      =   Helpers::GetRequest($request);

        if($heplers['status'] == false)
        {
            return \response()->json([
                'status'    =>  false,
                'error'   =>  $heplers['message'],
            ], 401);
        }else{

            parse_str($heplers['body']['data'], $params);

            if( ($params['name_users_edits'] == '') || ($params['email_users_edits'] == '') )
            {
                return \response()->json([
                    'status'    =>  false,
                    'token'     =>  $request->token,
                    'message'   =>  'Debe introducir un nombre o email valido'
                ], 200);
            }


            $_edit              =   FormsUsers::GetFormById($params['id_users_edits']);
            $_edit->username    =   strtolower($params['name_users_edits']);
            $_edit->email       =   strtolower($params['email_users_edits']);
            $_edit->name_id     =   $params['forms_users_edits'];
            $_edit->type_id     =   $params['type_users_edits'];
            $_edit->status_id   =   DataStatus::GetById($params['status_users_edits'])[0]->number;
            $_edit->save();

            return \response()->json([
                'status'    =>  true,
                'token'     =>  $request->token,
            ], 200);
        }
    }

    public function formsUsersRegister(Request $request)
    {
        $heplers      =   Helpers::GetRequest($request);

        if($heplers['status'] == false)
        {
            return \response()->json([
                'status'    =>  false,
                'error'   =>  $heplers['message'],
            ], 401);
        }else{

            parse_str($heplers['body']['data'], $params);

            if( ($params['name_users'] == '') || ($params['email_users'] == '') )
            {
                return \response()->json([
                    'status'    =>  false,
                    'token'     =>  $request->token,
                    'message'   =>  'Debe introducir un nombre o email valido'
                ], 200);
            }

            $_new   =   new FormsUsers;
            $_new->uuid         =   \Ramsey\Uuid\Uuid::uuid4()->toString();
            $_new->username     =   ucfirst($params['name_users']);
            $_new->email        =   strtolower($params['email_users']);
            $_new->name_id      =   $params['forms_users'];
            $_new->type_id      =   $params['type_users'];
            $_new->status_id    =   1;
            $_new->save();

            return \response()->json([
                'status'    =>  true,
                'token'     =>  $request->token,
            ], 200);
        }
    }

    public function formsEmailsEdit(Request $request)
    {
        $heplers      =   Helpers::GetRequest($request);

        if($heplers['status'] == false)
        {
            return \response()->json([
                'status'    =>  false,
                'error'   =>  $heplers['message'],
            ], 401);
        }else{

            return \response()->json([
                'status'    =>  true,
                'token'     =>  $request->token,
                'forms'     =>  FormsEmails::GetFormById($heplers['body']['id'])
            ], 200);
        }
    }

    public function formsEmailsUpdate(Request $request)
    {
        $heplers      =   Helpers::GetRequest($request);

        if($heplers['status'] == false)
        {
            return \response()->json([
                'status'    =>  false,
                'error'   =>  $heplers['message'],
            ], 401);
        }else{

            parse_str($heplers['body']['data'], $params);

            if( $params['e_name_emails'] == '')
            {
                return \response()->json([
                    'status'    =>  false,
                    'token'     =>  $request->token,
                    'message'   =>  'Debe introducir un email valido'
                ], 200);
            }

            $_edit              =   FormsEmails::GetFormById($params['id_e_name_emails']);
            $_edit->email       =   strtolower($params['e_name_emails']);
            $_edit->email_type  =   $params['e_type_emails'];
            $_edit->status_id   =   DataStatus::GetById($params['e_status_emails'])[0]->number;
            $_edit->form_id     =   $params['e_form_emails'];
            $_edit->form_name   =   FormsName::GetFormById($params['e_form_emails'])->name;

            $_edit->save();

            return \response()->json([
                'status'    =>  true,
                'token'     =>  $request->token,
            ], 200);
        }
    }

    public function formsEmailsRegister(Request $request)
    {
        $heplers      =   Helpers::GetRequest($request);

        if($heplers['status'] == false)
        {
            return \response()->json([
                'status'    =>  false,
                'error'   =>  $heplers['message'],
            ], 401);
        }else{

            parse_str($heplers['body']['data'], $params);

            if($params['name_emails'] == '')
            {
                return \response()->json([
                    'status'    =>  false,
                    'token'     =>  $request->token,
                    'message'   =>  'Debe introducir un email valido'
                ], 200);
            }

            $_new   =   new FormsEmails;
            $_new->uuid         =   \Ramsey\Uuid\Uuid::uuid4()->toString();
            $_new->email        =   strtolower($params['name_emails']);
            $_new->email_type   =   $params['type_emails'];
            $_new->status_id    =   1;
            $_new->form_id      =   $params['form_emails'];
            $_new->form_name    =   FormsName::GetFormById($params['form_emails'])->name;
            $_new->save();

            return \response()->json([
                'status'    =>  true,
                'token'     =>  $request->token,
            ], 200);
        }
    }

}
