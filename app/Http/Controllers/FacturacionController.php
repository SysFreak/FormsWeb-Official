<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

use App\Functions\DBMikroVE;
use App\Functions\Helpers;

use Carbon\Carbon;

use GoogleApi;
use Google\Client;
use Revolution\Google\Sheets\Sheets;

class FacturacionController extends Controller
{
	public function index(Request $request)
	{
        return view('facturacion.index');
	}

    public function api(Request $request)
    {
        $helpers    =   Helpers::GetRequest($request);

        $iClient    =   DBMikroVE::DBQuery('SELECT id, nombre, telefono FROM usuarios WHERE cedula LIKE "%'.$helpers['body']['ced'].'%"');

        
        if($iClient)
        {
            $cli    =   [
                'id'        =>  ($iClient['id'])        ? $iClient['id']        : '',
                'nombre'    =>  ($iClient['nombre'])    ? $iClient['nombre']    : '',
                'telefono'  =>  ($iClient['telefono'])  ? $iClient['telefono']  : '',
            ];
            return \response()->json([
                'status'    =>  true,
                'data'      =>  $cli
            ], 200);
        }

        return \response()->json([
            'status'    =>  false,
        ], 200);
        
    }

    public function forms(Request $request)
    {
        $helpers    =   Helpers::GetRequest($request);
        parse_str($helpers['body']['forms'], $params);
        
        switch ($helpers['body']['cal']) {
            case '1':   $cal = "01 - Muy insatisfecho"; break;
            case '2':   $cal = "02 - Insatisfecho";     break;
            case '3':   $cal = "03 - Deficiente";       break;
            case '4':   $cal = "04 - Irregular";        break;
            case '5':   $cal = "05 - Regular";          break;
            case '6':   $cal = "06 - Aceptable";        break;
            case '7':   $cal = "07 - Satisfecho";       break;
            case '8':   $cal = "08 - Muy satisfecho";   break;
            case '9':   $cal = "09 - Excelente";        break;
            case '10':  $cal = "10 - Lo recomiendo";    break;
            default:    $cal = "05 - Aceptable";        break;
        }

        switch ($helpers['body']['atc']) {
            case '1':   $atc = "Excelente";     break;
            case '2':   $atc = "Buena";         break;
            case '3':   $atc = "Deficiente";    break;
            default:    $atc = "Buena";         break;
        }

        switch ($helpers['body']['mb']) {
            case '1':   $mb = "Si";    break;
            case '2':   $mb = "No";    break;
            default:    $mb = "No";    break;
        }


        $client     =   new Client();
        $client->setAuthConfig($_SERVER['DOCUMENT_ROOT'].'/credentials.json');
        $client->setScopes([\Google\Service\Sheets::DRIVE, \Google\Service\Sheets::SPREADSHEETS]);
        $service    =   new \Google\Service\Sheets($client);

        $spreadsheetID 	    = 	'1PG1BugfRhytNy9tIkeI5H20vbntvkjYoZ1UIa2rJudE';
        $spreadsheetName    =   "Encuesta de Satisfaccion - BoomSolutions - VE";
        $range 			    =	'Data';

        $values	    =	[ [ strtoupper($params['mikro']),       strtoupper($params['titular']),    strtoupper($params['phone']),        strtoupper($params['ced']),
                            strtoupper($cal),                   strtoupper($atc),                  strtoupper($mb),
                            strtoupper($params['refName01']),   strtoupper($params['refTelf01']),  strtoupper($params['refName02']),    strtoupper($params['refTelf02']),
                            strtoupper($params['refName03']),   strtoupper($params['refTelf03']),  strtoupper($params['refName04']),    strtoupper($params['refTelf04']),
                            strtoupper($params['refName05']),   strtoupper($params['refTelf05']),  Carbon::now()->format('Y-m-d H:i:s')
                        ] ];

        $body 	    =	new \Google_Service_Sheets_ValueRange(['values'	=>	$values]);
        $params1 	=	[ 'valueInputOption'	=>	'RAW'];
        $insert 	=	[ "insertDataOption"	=>	"INSERT_ROWS"];
        $result 	=	$service->spreadsheets_values->append($spreadsheetID, $range, $body, $params1, $insert);

        $insert     =   DB::table('encuesta_satisfaccion')->insert([
            'mikrowisp'     => strtoupper($params['mikro']),
            'title'         => strtoupper($params['titular']),
            'phone'         => strtoupper($params['phone']),
            'ced'           => strtoupper($params['ced']),
            'calificacion'  => strtoupper($cal),
            'atencion'      => strtoupper($atc),
            'manitos'       => strtoupper($mb),
            'rfTitular1'    => strtoupper($params['refName01']),
            'rfPhone1'      => strtoupper($params['refTelf01']),
            'rfTitular2'    => strtoupper($params['refName02']),
            'rfPhone2'      => strtoupper($params['refTelf02']),
            'rfTitular3'    => strtoupper($params['refName03']),
            'rfPhone3'      => strtoupper($params['refTelf03']),
            'rfTitular4'    => strtoupper($params['refName04']),
            'rfPhone4'      => strtoupper($params['refTelf04']),
            'rfTitular5'    => strtoupper($params['refName05']),
            'rfPhone5'      => strtoupper($params['refTelf05']),
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $body       =   'Descriptci&oacute;n: Cliente deja la siguiente encuesta de satisfaccion. <br><br>

        Calificacion: '.strtoupper($cal).' <br>
        Atencion: '.strtoupper($atc).' <br>
        Manitos Boom: '.strtoupper($mb).' <br>

        Referidos: <br><br>

        Referido 1: '.strtoupper($params['refName01']).' <br>
        Telefono 1: '.strtoupper($params['refTelf01']).' <br>
        Referido 2: '.strtoupper($params['refName02']).' <br>
        Telefono 2: '.strtoupper($params['refTelf02']).' <br>
        Referido 3: '.strtoupper($params['refName03']).' <br>
        Telefono 3: '.strtoupper($params['refTelf03']).' <br>
        Referido 4: '.strtoupper($params['refName04']).' <br>
        Telefono 4: '.strtoupper($params['refTelf04']).' <br>
        Referido 5: '.strtoupper($params['refName05']).' <br>
        Telefono 5: '.strtoupper($params['refTelf05']).' <br>';

        $in      =  '&lt;p&gt;';
        $fn      =  '&lt;/p&gt;';

        $asunto     =   "Encuesta de Satisfaccion";

        $mensaje    =   ''.$in.' '.$body.' '.$fn.'';

        $query  =   'INSERT INTO notas(idcliente, asunto, mensaje, fecha, adjuntos, idoperador, imagen, tipo, portal) VALUES ("'.$params['mikro'].'", "'.$asunto.'", "'.$mensaje.'", NOW(), "", "102", "", "0", "")';
        $nota   =   DBMikroVE::DataExecute($query);

        return \response()->json([
            'status'    =>  true,
        ], 200);

    }
}
