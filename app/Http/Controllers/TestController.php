<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

use App\Models\FormsName;
use App\Models\FormsUsers;
use App\Models\BasicForm;
use App\Models\DataStatusEmail;
use App\Models\FormsEmails;

use App\Functions\Helpers;
use App\Functions\DBMikroVE;

use GoogleApi;
use Google\Client;
use Revolution\Google\Sheets\Sheets;

use Illuminate\Http\Request;

class TestController extends Controller
{
	public function index(Request $request)
	{

        dd(date("Y-m-d H:m:s"));

        dd("TestController");
	}

}
