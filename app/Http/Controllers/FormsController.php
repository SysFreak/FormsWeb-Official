<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Str;

use App\Functions\Helpers;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Models\FormsName;
use App\Models\FormsType;
use App\Models\FormsUsers;
use App\Models\FormsEmails;

use App\Models\DataCeilings;
use App\Models\DataCountry;
use App\Models\DataLevels;
use App\Models\DataServices;
use App\Models\DataTerms;
use App\Models\DataPlans;

use App\Models\BasicForm;
use App\Models\FinishForm;
use App\Models\DataStatusEmail;

use App\Bitrix24\Bitrix24API;
use App\Bitrix24\Bitrix24APIException;
use Carbon\Carbon;

use GoogleApi;
use Google\Client;
use Revolution\Google\Sheets\Sheets;

class FormsController extends Controller
{
    /**
     * Show View Solicitud.
     *
     * @return \Illuminate\Http\Response
     */

    public function solicitud(Request $request)
    {
        $form   =   substr($request->fullUrl(), strlen($_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/') ,strlen($request->fullUrl()) );
        $users  =   $fName  =   null;

        $fName  =   FormsName::GetUsersForms($form);
        $users  =   ($fName[0]->users_id == 1) ?  FormsUsers::GetUsersFormsId($fName[0]->uuid) : null;

        return view('solicitud.index', [
            'username'  =>  $form,
            'form'      =>  $fName[0]->uuid,
            'users'     =>  $users,
            'pass'      =>  $fName[0]->password,
        ]);
    }

    /**
     * Show View External.
     *
     * @return \Illuminate\Http\Response
     */

    public function external(Request $request)
    {
        $users  =   $fName  =   null;

        $fName  =   FormsName::GetUsersForms('external');
        $users  =   ($fName[0]->users_id == 1) ?  FormsUsers::GetUsersFormsId($fName[0]->uuid) : null;

        return view('solicitud.index', [
            'username'  =>  'external',
            'form'      =>  $fName[0]->uuid,
            'users'     =>  $users,
            'pass'      =>  $fName[0]->password,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function load(Request $request)
    {

        $terms      =   Helpers::ComplexStatusTrue(DataTerms::all());
        $country    =   Helpers::ComplexStatusTrue(DataCountry::all());
        $ceiling    =   Helpers::ComplexStatusTrue(DataCeilings::all());
        $levels     =   Helpers::ComplexStatusTrue(DataLevels::all());
        $plans      =   Helpers::ComplexStatusTrue(DataPlans::all());
        $services   =   Helpers::ComplexStatusTrue(DataServices::all());

        return \response()->json([
            'success'   => true,
            'country'   =>  ($country['status'])    ? $country['data']  : "",
            'ceilings'  =>  ($ceiling['status'])    ? $ceiling['data']  : "",
            'levels'    =>  ($levels['status'])     ? $levels['data']   : "",
            'terms'     =>  ($terms['status'])      ? $terms['data']    : "",
            'plans'     =>  ($plans['status'])      ? $plans['data']    : "",
            'services'  =>  ($services['status'])   ? $services['data'] : "",
        ], Response::HTTP_OK);
    }

    public function basicforms(Request $request)
    {
        $helpers    =   Helpers::GetRequest($request);

        unset($helpers['body']['pass_form']);
        unset($helpers['body']['username_form']);

        if($helpers['status'] == true)
        {
            $forms   =   new BasicForm;

            foreach($helpers['body'] as $k => $val)  { if ( !preg_match("/term/i", $k) ) { $forms[$k] = strtoupper($val); } }
            $forms['uuid']      =   Str::uuid()->toString();

            if(!isset($forms->users_form)){ $forms['users_form'] = "0"; }

            $state = DataCountry::GetByID($forms->state);

            if($forms->save())
            {

                if(($state == "Lara") || ($state == "Yaracuy"))
                {
                    return \response()->json([
                        'status'    =>  true,
                        'next'      =>  true,
                        'id'        =>  $forms['uuid'],
                    ], 200);
                }else{
                    return \response()->json([
                        'status'    =>  true,
                        'next'      =>  false,
                    ], 200);
                }
            }else{
                return \response()->json([
                    'status'    =>  false,
                    'message'   =>  $heplers['message'],
                ], 401);
            }

            return \response()->json($forms, Response::HTTP_OK);

        }else{
            return \response()->json([
                'status'    =>  false,
                'message'   =>  $heplers['message'],
            ], 401);

        }
    }

    public function finishforms(Request $request)
    {
        $helpers    =   Helpers::GetRequest($request);

        if($helpers['status'] == true)
        {
            $forms   =   new FinishForm;
            foreach($helpers['body'] as $k => $val)  { if ( !preg_match("/term/i", $k) ) { $forms[$k] = strtoupper($val); } }
            $forms['uuid']    =   Str::uuid()->toString();

            if($forms->save())
            {
                return \response()->json([
                    'status'    =>  true,
                ], 200);

            }else{
                return \response()->json([
                    'status'    =>  false,
                ], 401);
            }

        }else{
            return \response()->json([
                'status'    =>  false,
                'message'   =>  $heplers['message'],
            ], 401);

        }

        return \response()->json($helpers, Response::HTTP_OK);
    }

    public function formscurl(Request $request)
    {
        $helpers    =   Helpers::GetRequest($request);

        if($helpers['status'] == false)
        {

            return \response()->json([
                'status'    =>  false,
                'error'   =>  $heplers['message'],
            ], 401);

        }else{
            $add 	=	str_replace(" ", "", $request->add);

            $url 	=	'https://maps.google.com/maps/api/geocode/json?address='.$add.'&key=AIzaSyBmScbfZ1jRKJH262lqWAfX_HGBbgra1Ao&sensor=false';

            $ch 	=	curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $response_a =	json_decode($response);

            if($response_a->status == 'OK')
            {
                return \response()->json([
                    'status'    =>  true,
                    'latitud'   =>  $response_a->{'results'}[0]->geometry->location->lat,
                    'longitud'  =>  $response_a->{'results'}[0]->geometry->location->lng,
                ], 200);
            }else{
                return \response()->json([
                    'status'    =>  false
                ], 401);
            }
        }

    }
}
