<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use App\Functions\Helpers;
use App\Bitrix24\Bitrix24API;
use App\Bitrix24\Bitrix24APIException;


use Carbon\Carbon;

class MassiveLeadsController extends Controller
{
    public function index(Request $request)
    {
        return view('panel.massive.index',[
            'username'  =>  session('name'),
            'token'     =>  session('id'),
        ]);
    }

    public function upload(Request $request)
    {
        $name       =   $request->file('file')->getClientOriginalName();

        if( ($open = fopen($request->file('file'), "r")) !== FALSE )
        {
            while ( ($data = fgetcsv($open, 1000, ",")) !== FALSE ) 
            {
                $name   =   explode(" ", $data[0]);

                DB::table('b24_clients_tmp')->insert([
                    'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
                    'name'          => strtoupper($name[0]),
                    'last_name'     => strtoupper($name[1]),
                    'phone'         => $data[1],
                    'alternative'   => $data[2],
                    'email'         => strtolower($data[3]),
                    'address'       => strtoupper($data[4]),
                    'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
                ]);
            }
            
            fclose($open);

            $data   =   DB::table('b24_clients_tmp')->orderBy('id', 'ASC')->get();
            $iData  =   [];
    
            foreach ($data as $k => $val) 
            {
                $iData[$val->id] =  [
                    'id'        =>  $val->id,
                    'name'      =>  $val->name .' '. $val->last_name,
                    'phone'     =>  $val->phone,
                    'status'    =>  ( ($val->status_id == 0) ? 'PENDIENTE' : 'PROCESADO' )
                ];
            }         

            return \response()->json([
                'success'   =>  true,
                'data'      =>  $iData
            ], Response::HTTP_OK);
        }
       
        return \response()->json([
            'success'   => false,
        ], Response::HTTP_OK);
    }

    public function load (Request $request)
    {

        $data   =   DB::table('b24_clients_tmp')->orderBy('id', 'ASC')->get();

        if($data)
        {
            $iData  =   [];
    
            foreach ($data as $k => $val) 
            {
                $iData[$val->id] =  [
                    'id'        =>  $val->id,
                    'name'      =>  $val->name .' '. $val->last_name,
                    'phone'     =>  $val->phone,
                    'status'    =>  ( ($val->status_id == 0) ? 'PENDIENTE' : 'PROCESADO' )
                ];
            }         

            return \response()->json([
                'success'   =>  true,
                'data'      =>  $iData
            ], Response::HTTP_OK);

        }else{
            return \response()->json([
                'success'   =>  false,
            ], Response::HTTP_OK);
        }
    }

    public function process(Request $request)
    {
        $data           =   DB::table('b24_clients_tmp')->where('status_id', '=', '0')->get();

        $webhookURL     = 	'https://boomsolutions.bitrix24.com/rest/47503/l0f9htbyj4tj1hnj/';

        $B24 		    = 	new Bitrix24API($webhookURL);

        $dataB24    =   $B24->addLead([
            'TITLE'      => 'PRUEBA',
            'COMPANY_ID' => 6,
            'CONTACT_ID' => 312
        ]);

        var_dump($dataB24);exit;
        
        foreach ($data as $d => $dat) 
        {
            $clientB24      =   $B24->addLead(Helpers::InfoMassiveClient($dat));

            var_dump($clientB24);exit;
            
        }


    }
}
