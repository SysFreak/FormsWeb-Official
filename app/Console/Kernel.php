<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Console\Commands\SingleMail;
use App\Console\Commands\CompleteMail;
use App\Console\Commands\EmailMarketing;
use App\Console\Commands\MkwVeFacturacion;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        // $schedule->command('single:mail')->everyTenMinutes();
        $schedule->command('single:mail')->everyTenMinutes();
        $schedule->command('complete:mail')->everyThirtyMinutes();
        // $schedule->command('command:payment')->everyTwoMinutes();
        // $schedule->command('command:payment')->daily();
        // $schedule->command('complete:mail')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
