<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Models\FormsName;
use App\Models\FormsUsers;
use App\Models\BasicForm;
use App\Models\DataStatusEmail;
use App\Models\FormsEmails;

use App\Functions\Helpers;

use GoogleApi;
use Google\Client;
use Revolution\Google\Sheets\Sheets;

use App\Bitrix24\Bitrix24API;
use App\Bitrix24\Bitrix24APIException;

use Carbon\Carbon;

class SingleMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'single:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CronJob to send email every 5 minuntos. Simple Information.!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $forms  =   FormsName::GetFormActive();

        if($forms)
        {
            foreach ($forms as $f => $form)
            {
                $iData   =   BasicForm::GetFormByName($form->uuid);

                if($form)
                {
                    foreach ($forms as $f => $form)
                    {
                        $iData   =   BasicForm::GetFormByName($form->uuid);

                        if($iData)
                        {
                            foreach ($iData as $iD => $ida)
                            {

                                $iEmail     =   DataStatusEmail::GetBasicForm($ida->uuid);

                                if($iEmail == null)
                                {

                                    $sEmail     =   DataStatusEmail::InsertBasicEmail($ida->uuid);

                                    if($sEmail)
                                    {

                                        $query      =   DB::table('basic_forms')
                                        ->join('data_countries',    'basic_forms.state',    '=',    'data_countries.uuid')
                                        ->join('data_levels',       'basic_forms.level',    '=',    'data_levels.uuid')
                                        ->join('data_ceilings',     'basic_forms.ceiling',  '=',    'data_ceilings.uuid')
                                        ->where('basic_forms.uuid',  '=',    $ida->uuid)
                                        ->select('basic_forms.name as Nombre',  'basic_forms.last_name as Apellido',    'basic_forms.cedula as Cedula',
                                        'basic_forms.age as Nacimiento',    'basic_forms.email as Email',       'basic_forms.phone_prin as Principal',
                                        'basic_forms.phone_alt as Alterno', 'data_countries.name as Estado',    'basic_forms.city as Ciudad',
                                        'basic_forms.postal as Postal',     'basic_forms.address as Direccion',  'basic_forms.reference as Referencia',
                                        'basic_forms.latitud as Latitud',   'basic_forms.longitud as Longitud',     'data_levels.name as Niveles',
                                        'data_ceilings.name as Techo',      'basic_forms.created_at as Creacion', 'basic_forms.users_form AS Operador')
                                        ->first();

                                        if($query->Operador == "0") { $user   =   ""; }else{ $user   =   FormsUsers::GetUserId($query->Operador)->email; }
                                        $query->Vendedor  =   $user;
                                        foreach($query as $k => $val)  { if ( !preg_match("/Operador/", $k) ) { $querys[$k] = $val; } }

                                        $webhookURL     = 	'https://boomsolutions.bitrix24.com/rest/47503/l0f9htbyj4tj1hnj/';

                                        $B24 		    = 	new Bitrix24API($webhookURL);
                                        // $clientB24      =   $B24->addLead(Helpers::InfoClient($query));
                                        $clientB24      =   $ida->uuid;

                                        if($clientB24)
                                        {
                                            \Log::info(date("Y-m-d h:m:s")." - Bitrix24 - ".$clientB24." - Leads - Status: Success - Data Client: Tipo de Formulario: ".ucfirst($form->name)." - Name: ".ucfirst($querys['Nombre'])." ".ucfirst($querys['Apellido'])." - Telefono: ".ucfirst($querys['Principal'])." - Ciudad: ".ucfirst($querys['Ciudad'])." !");
                                        }else{
                                            \Log::info(date("Y-m-d h:m:s")." - Bitrix24 - Status: Error - Data Client: Tipo de Formulario: ".ucfirst($form->name)." - Name: ".ucfirst($querys['Nombre'])." ".ucfirst($querys['Apellido'])." - Telefono: ".ucfirst($querys['Principal'])." - Ciudad: ".ucfirst($querys['Ciudad'])." !");
                                        }

                                        $client     =   new Client();
                                        // $client->setAuthConfig($_SERVER['DOCUMENT_ROOT'].'/../credentials.json');
                                        $client->setAuthConfig($_SERVER['PWD'].'/credentials.json');
                                        $client->setScopes([\Google\Service\Sheets::DRIVE, \Google\Service\Sheets::SPREADSHEETS]);
                                        $service    =   new \Google\Service\Sheets($client);
                                        $spreadsheetID 	    = 	'15CpOI71HMGlxVswizXz0fOt7Sf1jxkT0CKGhOqzrR0w';
                                        $spreadsheetName    =   "Solicitudes de Formularios Web - General";
                                        $range 			    =	'Leads';

                                        $values	    =	[ [ ucfirst($querys['Nombre']),          ucfirst($querys['Apellido']),    ucfirst($querys['Cedula']),      ucfirst($querys['Nacimiento']),
                                                            ucfirst($querys['Email']),           ucfirst($querys['Principal']),   ucfirst($querys['Alterno']),     ucfirst($querys['Estado']),
                                                            ucfirst($querys['Ciudad']),          ucfirst($querys['Postal']),      ucfirst($querys['Direccion']),   ucfirst($querys['Referencia']),
                                                            ucfirst($querys['Latitud']),        ucfirst($querys['Longitud']),     ucfirst($querys['Niveles']),     ucfirst($querys['Techo']),
                                                            ucfirst($form->name),               'Leads',                          ucfirst($querys['Vendedor']),    ucfirst($querys['Creacion'])
                                                        ] ];

                                        $body 	    =	new \Google_Service_Sheets_ValueRange(['values'	=>	$values]);
                                        $params 	=	[ 'valueInputOption'	=>	'RAW'];
                                        $insert 	=	[ "insertDataOption"	=>	"INSERT_ROWS"];
                                        $result 	=	$service->spreadsheets_values->append($spreadsheetID, $range, $body, $params, $insert);

                                        if($result)
                                        {
                                            \Log::info(date("Y-m-d h:m:s")." - Google Sheet - ".$spreadsheetName." - Leads - Status: Success - Data Client: Tipo de Formulario: ".ucfirst($form->name)." - Name: ".ucfirst($querys['Nombre'])." ".ucfirst($querys['Apellido'])." - Telefono: ".ucfirst($querys['Principal'])." - Ciudad: ".ucfirst($querys['Ciudad'])." !");
                                        }else{
                                            \Log::info(date("Y-m-d h:m:s")." - Google Sheet - ".$spreadsheetName." - Leads - Status: Error - Data Client: Tipo de Formulario: ".ucfirst($form->name)." - Name: ".ucfirst($querys['Nombre'])." ".ucfirst($querys['Apellido'])." - Telefono: ".ucfirst($querys['Principal'])." - Ciudad: ".ucfirst($querys['Ciudad'])." !");
                                        }


                                        $emails     =   FormsEmails::GetEmailsByID(['id' => $form->uuid, 'type' => '1']);
                                        $ccs        =   FormsEmails::GetEmailsByID(['id' => $form->uuid, 'type' => '2']);

                                        Mail::to($emails)->cc($ccs)->send(new \App\Mail\SingleMail(['type' => 'single', 'name' => $form->name, 'data' => $querys]));

                                        if (Mail::failures())
                                        {
                                            \Log::info(date("Y-m-d h:m:s")." - Email - Single Status: No Send - Mail Type: ".$form->name." - Name: ".$querys['Nombre']." ".$querys['Apellido']." Telefono: ".$querys['Principal']." Ciudad: ".$querys['Ciudad']." !");

                                        }else{

                                            \Log::info(date("Y-m-d h:m:s")." - Email - Single Status: Success - Mail Type: ".$form->name." - Name: ".$querys['Nombre']." ".$querys['Apellido']." Telefono: ".$querys['Principal']." Ciudad: ".$querys['Ciudad']." !");

                                        }
                                    }else{

                                        \Log::info(date("Y-m-d h:m:s")." - Register Fails - ID:  ".$ida->uuid."");
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
    }
}
