<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Models\FormsName;
use App\Models\BasicForm;
use App\Models\FinishForm;
use App\Models\DataStatusEmail;
use App\Models\FormsEmails;
use App\Models\FormsUsers;

use GoogleApi;
use Google\Client;
use Revolution\Google\Sheets\Sheets;

class CompleteMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'complete:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CronJob to send email every hours. Complete Information.!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $iEmail     =   FinishForm::GetEmails();

        if($iEmail)
        {
            foreach ($iEmail as $i => $ime)
            {
                $iMails     =   DataStatusEmail::GetFinishForm($ime->form_id);

                if($iMails != null)
                {
                    $sEmail     =   DataStatusEmail::UpdateFinishEmail(['form_id' => $ime->form_id, 'next_id' => $ime->uuid]);

                    if($sEmail)
                    {
                        $query  =   DB::table('basic_forms')
                            ->join('finish_forms',      'basic_forms.uuid',             '=',    'finish_forms.form_id')
                            ->join('data_countries',    'basic_forms.state',            '=',    'data_countries.uuid')
                            ->join('data_levels',       'basic_forms.level',            '=',    'data_levels.uuid')
                            ->join('data_ceilings',     'basic_forms.ceiling',          '=',    'data_ceilings.uuid')
                            ->join('data_services',     'finish_forms.type_service',    '=',    'data_services.id')
                            ->join('data_plans',        'finish_forms.list_plan',       '=',    'data_plans.uuid')
                            ->where('basic_forms.uuid',  '=',    $ime->form_id)
                            ->select('basic_forms.name as Nombre',          'basic_forms.last_name as Apellido',        'basic_forms.cedula as Cedula',
                                    'basic_forms.age as Nacimiento',        'basic_forms.email as Email',               'basic_forms.phone_prin as Principal',
                                    'basic_forms.phone_alt as Alterno',     'data_countries.name as Estado',            'basic_forms.city as Ciudad',
                                    'basic_forms.postal as Postal',         'basic_forms.address as Direccion',         'basic_forms.reference as Referencia',
                                    'basic_forms.latitud as Latitud',       'basic_forms.longitud as Longitud',         'data_levels.name as Niveles',
                                    'data_ceilings.name as Techo',
                                    'data_services.name AS Servicio',       'data_plans.name AS Plan',                  'finish_forms.name_com AS Comercial',
                                    'finish_forms.rif_com AS Rif',          'finish_forms.rep_com AS Representante',    'finish_forms.email_com AS Correo',
                                    'finish_forms.provider AS Proveedor',   'finish_forms.ups AS Ups',                  'finish_forms.covid AS Covid',
                                    'basic_forms.created_at as Creacion',   'basic_forms.users_form AS Operador')
                            ->first();

                        if($query->Operador == "0") { $user   =   ""; }else{ $user   =   FormsUsers::GetUserId($query->Operador)->email; }
                        $query->Vendedor  =   $user;
                        foreach($query as $k => $val)  { if ( !preg_match("/Operador/", $k) ) { $querys[$k] = $val; } }

                        $forms  =   BasicForm::GetNameFormFinish($ime->form_id);

                        $client     =   new Client();
                        $client->setAuthConfig($_SERVER['PWD'].'/credentials.json');
                        $client->setScopes([\Google\Service\Sheets::DRIVE, \Google\Service\Sheets::SPREADSHEETS]);
                        $service    =   new \Google\Service\Sheets($client);
                        $spreadsheetID 	    = 	'15CpOI71HMGlxVswizXz0fOt7Sf1jxkT0CKGhOqzrR0w';
                        $spreadsheetName    =   "Solicitudes de Formularios Web - General";
                        $range 			    =	'Complete';

                        $values	    =	[ [ ucfirst($querys['Nombre']),         ucfirst($querys['Apellido']),    ucfirst($querys['Cedula']),        ucfirst($querys['Nacimiento']),
                                            ucfirst($querys['Email']),          ucfirst($querys['Principal']),   ucfirst($querys['Alterno']),       ucfirst($querys['Estado']),
                                            ucfirst($querys['Ciudad']),         ucfirst($querys['Postal']),      ucfirst($querys['Direccion']),     ucfirst($querys['Referencia']),
                                            ucfirst($querys['Latitud']),       ucfirst($querys['Longitud']),
                                            ucfirst($querys['Niveles']),        ucfirst($querys['Techo']),       ucfirst($querys['Servicio']),      ucfirst($querys['Plan']),
                                            ucfirst($querys['Comercial']),      ucfirst($querys['Rif']),         ucfirst($querys['Representante']), ucfirst($querys['Correo']),
                                            ucfirst($querys['Proveedor']),      ucfirst($querys['Ups']),         ucfirst($querys['Covid']),         ucfirst($forms->name),
                                            'Complete',                         ucfirst($querys['Vendedor']),    ucfirst($querys['Creacion'])
                                        ] ];

                        $body 	    =	new \Google_Service_Sheets_ValueRange(['values'	=>	$values]);
                        $params 	=	[ 'valueInputOption'	=>	'RAW'];
                        $insert 	=	[ "insertDataOption"	=>	"INSERT_ROWS"];
                        $result 	=	$service->spreadsheets_values->append($spreadsheetID, $range, $body, $params, $insert);

                        if($result)
                        {
                            \Log::info(date("Y-m-d h:m:s")." - Google Sheet - ".$spreadsheetName." - Complete - Status: Success - Data Client: Tipo de Formulario: ".ucfirst($forms->name)." - Name: ".ucfirst($querys['Nombre'])." ".ucfirst($querys['Apellido'])." - Telefono: ".ucfirst($querys['Principal'])." - Ciudad: ".ucfirst($querys['Ciudad'])." !");
                        }else{
                            \Log::info(date("Y-m-d h:m:s")." - Google Sheet - ".$spreadsheetName." - Complete - Status: Error - Data Client: Tipo de Formulario: ".ucfirst($forms->name)." - Name: ".ucfirst($querys['Nombre'])." ".ucfirst($querys['Apellido'])." - Telefono: ".ucfirst($querys['Principal'])." - Ciudad: ".ucfirst($querys['Ciudad'])." !");
                        }

                        $emails     =   FormsEmails::GetEmailsByID(['id' => $forms->uuid, 'type' => '1']);
                        $ccs        =   FormsEmails::GetEmailsByID(['id' => $forms->uuid, 'type' => '2']);

                        Mail::to($emails)->cc($ccs)->send(new \App\Mail\CompleteMail(['type' => 'complete', 'name' => $forms->name, 'data' => $querys]));

                        if (Mail::failures())
                        {
                            \Log::info(date("Y-m-d h:m:s")." - Email - Complete Status: No Send - Mail Type: ".ucfirst($forms->name)." - Name: ".ucfirst($querys['Nombre'])." ".ucfirst($querys['Apellido'])." - Telefono: ".ucfirst($querys['Principal'])." - Ciudad: ".ucfirst($querys['Ciudad'])." !");

                        }else{
                            \Log::info(date("Y-m-d h:m:s")." - Email - Complete Status: Success - Mail Type: ".ucfirst($forms->name)." - Name: ".ucfirst($querys['Nombre'])." ".ucfirst($querys['Apellido'])." - Telefono: ".ucfirst($querys['Principal'])." - Ciudad: ".ucfirst($querys['Ciudad'])." !");
                        }

                    }else{
                        \Log::info(date("Y-m-d h:m:s")." - Register Fails - ID:  ".$ime->form_id."");
                    }
                }
            }
        }
    }
}
