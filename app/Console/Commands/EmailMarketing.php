<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class EmailMarketing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:marketing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CronJob to send email marketing.!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $query      =   DB::table('email_data_cp')->where('email_status', '=', 'no')->select('email_id', 'email_name', 'email_body', 'email_address', 'email_track_code')->get();
        // $query      =   DB::table('email_data')->where('email_status', '=', 'no')->select('email_id', 'email_name', 'email_body', 'email_address', 'email_track_code')->get();

        foreach ($query as $k => $val)
        {
            $iData =    [
                'title' =>  $val->email_body.' - '.$val->email_name,
                'name'  =>  $val->email_name,
                'track' =>  $val->email_track_code,
                'web'   =>  'https://test.boomsolutions.com/'
            ];

            // Mail::to(trim($val->email_address))->send(new \App\Mail\EmailMarketing($iData));
            Mail::to(trim($val->email_address))->send(new \App\Mail\EnvioRetencion($iData));

            if (Mail::failures()) {
                \Log::info(date("Y-m-d H:m:s")." - Email - Single Status: No Send - ".$val->email_name." - ".$val->email_address);

            }else{
                $query2  =   DB::table('email_data_cp')->where('email_id', '=', $val->email_id)->update( ['email_status' => 'yes', 'email_send_datetime' => date("Y-m-d H:m:s") ] );
                // $query2  =   DB::table('email_data')->where('email_id', '=', $val->email_id)->update( ['email_status' => 'yes', 'email_send_datetime' => date("Y-m-d H:m:s") ] );
                \Log::info(date("Y-m-d H:m:s")." - Email - Single Status: Success - ".$val->email_name." - ".$val->email_address);
             }

             sleep(15);

        }

    }
}
