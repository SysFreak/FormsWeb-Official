<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use App\Functions\DBSmart;
use App\Functions\DBMikroVE;

use Carbon\Carbon;

class MkwVeFacturacion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:payment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clase para realizar la facturacion paralela a Mikrowisp Venezuela';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $idClient   =   DBMikroVE::DBQueryAll('SELECT id FROM usuarios WHERE (estado = "ACTIVO" OR estado = "SUSPENDIDO") ORDER BY id ASC');

        foreach ($idClient as $k => $idd)
        {

            $id     =   $idd['id'];

            $iOtros         =   $iServ  =   $iServOtro   =   $iInter     =   $iOnePay  =    $iSaldo     =   $iReconexion    =   [];
            $iTotalServRec  =   $iTotalServRecOtro       =   $iTotalServRecOtro =   $iOnePayTotal    =   $sFavor    =   $sDeudor    =   $iTotalInter     =  $iRecoTotal   =   0;
            $iServRecTotal  =   0;

            $inter  =   DBMikroVE::DBQueryAll('SELECT id, descripcion, costo, ip FROM tblservicios WHERE idcliente = "'.$id.'" ORDER BY id ASC');

            if($inter <> false)
            {
                foreach ($inter as $o => $ot) 
                {
                    $iInter[$o] =   [
                        'ID'            =>  $ot['id'],
                        'DESCRIPCION'   =>  $ot['descripcion'],
                        'COSTO'         =>  round($ot['costo'],2),
                        'IMPUESTO'      =>  "16.00",
                        'IP'            =>  $ot['ip'],
                        'TOTAL'         =>  round(( round($ot['costo'],2) + (round($ot['costo'],2) * (round("16.00",2) / 100) )),2)
                    ];

                    $iTotalInter   =   $iTotalInter + $iInter[$o]['TOTAL'];
                }
            
            }

            $servs  =   DBMikroVE::DBQueryAll('SELECT a.id, p.producto, p.descripcion, a.monto, p.impuesto FROM alquileres a INNER JOIN almacen al ON (a.productoid = al.id) INNER JOIN productos p ON (al.productoid = p.id) AND a.userid = "'.$id.'" AND a.tipoa = "producto" AND a.state = "activo"');

            if($servs <> false)
            {
                foreach ($servs as $o => $ot) 
                {
                    $iServRec[$o] =   [
                        'ID'            =>  $ot['id'],
                        'PRODUCTO'      =>  $ot['producto'],
                        'DESCRIPCION'   =>  $ot['descripcion'],
                        'MONTO'         =>  round($ot['monto'],2),
                        'IMPUESTO'      =>  round($ot['impuesto'],2),
                        'TOTAL'         =>  round(( round($ot['monto'],2) + (round($ot['monto'],2) * (round($ot['impuesto'],2) / 100) )),2)
                    ];

                    $iTotalServRec  =   $iTotalServRec + $iServRec[$o]['TOTAL'];
                }
            
            }

            $otros  =   DBMikroVE::DBQueryAll('SELECT a.id, p.producto, p.descripcion, a.monto, p.impuesto FROM alquileres a INNER JOIN productos p ON (a.productoid = p.id) AND a.userid = "'.$id.'" AND a.state = "activo"');

            if($otros <> false)
            {
                foreach ($otros as $o => $ot) 
                {
                    $iServRecOtro[$o] =   [
                        'ID'            =>  $ot['id'],
                        'PRODUCTO'      =>  $ot['producto'],
                        'DESCRIPCION'   =>  $ot['descripcion'],
                        'MONTO'         =>  round($ot['monto'],2),
                        'IMPUESTO'      =>  round($ot['impuesto'],2),
                        'TOTAL'         =>  round(( round($ot['monto'],2) + (round($ot['monto'],2) * (round($ot['impuesto'],2) / 100) )),2)
                    ];

                    $iTotalServRecOtro     =   $iTotalServRecOtro + $iServRecOtro[$o]['TOTAL'];
                }
            
            }

            $iServRecTotal  =   $iServRecTotal + $iTotalServRecOtro;

            $temp   =   DBMikroVE::DBQueryAll('SELECT t.id, p.producto, p.descripcion, t.monto FROM tmpfactura t INNER JOIN productos p ON (t.productoid = p.id) AND t.userid = "'.$id.'" AND state = "PENDIENTE" ORDER BY id ASC');

            if($temp <> false)
            {
                foreach ($temp as $o => $ot) 
                {
                    $iOnePay[$o] =   [
                        'ID'            =>  $ot['id'],
                        'PRODUCTO'      =>  $ot['producto'],
                        'DESCRIPCION'   =>  $ot['descripcion'],
                        'COSTO'         =>  round($ot['monto'],2)
                    ];

                    $iOnePayTotal   =   $iOnePayTotal + $iOnePay[$o]['COSTO'];
                }
            
            }

            $saldos =   DBMikroVE::DBQueryAll('SELECT * FROM saldos WHERE iduser = "'.$id.'" AND estado = "no cobrado"');

            if($saldos <> false)
            {
                foreach ($saldos as $o => $ot) 
                {
                    if($ot['monto'] < 0)
                    {
                        $sFavor     =   $sFavor + $ot['monto'];
                    }else{
                        $sDeudor    =   $sDeudor + $ot['monto'];
                    }
                }
            
            }

            $reconexion    =    DBMikroVE::DBQueryAll('SELECT * FROM reconexiones WHERE  cliente = "'.$id.'" AND estado = "1"');

            if($reconexion <> false)
            {
                foreach ($reconexion as $o => $ot) 
                {
                    $iReconexion[$o] =   [
                        'ID'            =>  $ot['id'],
                        'DESCRIPCION'   =>  $ot['detalle'],
                        'COSTO'         =>  round($ot['monto_reconexion'],2)
                    ];

                    $iRecoTotal   =   $iRecoTotal + $iReconexion[$o]['COSTO'];
                }
            
            }

            $tFactPar       =   $iTotalInter + $iServRecTotal + $iOnePayTotal + $sDeudor + $iRecoTotal;
            $tFactTotal     =   ( ($tFactPar + $sFavor) < 0) ? "0.00" : ($tFactPar + $sFavor);

            $iGeneral[$k]   =   [
                'client'        =>  $id,
                'plan'          =>  $iTotalInter,
                'servicios'     =>  $iTotalServRec,
                'servicios_o'   =>  $iTotalServRecOtro,
                'sFavor'        =>  $sFavor,
                'sDeudor'       =>  $sDeudor,
                'sReconexion'   =>  $iRecoTotal,
                'tParcial'      =>  $tFactPar,
                'tFactura'      =>  $tFactTotal,
            ];

            $insert     =   DB::table('fact_mikro_ve_tmp')->insert([
                'cliente'           => $iGeneral[$k]['client'],
                'plan'              => $iGeneral[$k]['plan'],
                'servicios'         => $iGeneral[$k]['servicios'],
                'servicios_otros'   => $iGeneral[$k]['servicios_o'],
                'saldo_favor'       => $iGeneral[$k]['sFavor'],
                'saldo_deudor'      => $iGeneral[$k]['sDeudor'],
                'saldo_reconexion'  => $iGeneral[$k]['sReconexion'],
                'total_parcial'     => $iGeneral[$k]['tParcial'],
                'total_factura'     => $iGeneral[$k]['tFactura'],
                'created_at'        => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
            
            \Log::info( date("Y-m-d h:m:s")." - Process Payment Mikrowisp - Status: Susscess - Client: ".ucfirst($id)." " );
        
        }
    }
}
