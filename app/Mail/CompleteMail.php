<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CompleteMail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details  =   $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $text   =   "Formulario de Ventas - " . ucfirst( $this->details['data']['Nombre'] ) . " " . ucfirst( $this->details['data']['Apellido'] ) . " - Complete - " . ucfirst( $this->details['name'] ) . "";

        return $this->subject($text)->view('mails.single');
    }
}
