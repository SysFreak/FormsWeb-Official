<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataCountry extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    public $fillable = ['id', 'uuid', 'name', 'status_id', 'created_at'];

    public static function GetByID($id)
    {
        return  DataCountry::where('uuid', '=', $id)->firstOrFail()->name;
    }

}
