<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FormsName extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    public $fillable = ['id', 'uuid', 'name', 'type_id', 'status_id', 'created_at'];

    public static function GetUsersForms($forms)
    {
        return DB::table('forms_names')->join('forms_types', 'forms_names.type_id', '=', 'forms_types.uuid')
                ->where('forms_names.name', '=', $forms)
                ->select('forms_names.uuid', 'forms_types.users_id', 'forms_names.password')
                ->get();
    }

    public static function GetUsersFormsId($forms)
    {
        return DB::table('forms_names')->join('forms_types', 'forms_names.type_id', '=', 'forms_types.uuid')
                ->where('forms_names.uuid', '=', $forms)
                ->select('forms_names.id', 'forms_names.uuid', 'forms_names.name', 'forms_types.users_id', 'forms_names.password')
                ->first();
    }

    public static function GetFormsTable()
    {
        return DB::table('forms_names')->join('forms_types', 'forms_names.type_id', '=', 'forms_types.uuid')
                ->join('data_status', 'forms_names.status_id', '=', 'data_status.number')
                ->where('forms_names.status_id','=', 1)
                ->select('forms_names.id', 'forms_names.uuid', 'forms_names.name','forms_types.name as type', 'data_status.name as status')
                ->orderBy('forms_names.id', 'DESC')
                ->get();
    }

    public static function GetFormById($id)
    {
        return FormsName::where('uuid', '=', $id)->first();
    }

    public static function GetFormID($id)
    {
        return FormsName::where('id', '=', $id)->first();
    }

    public static function GetFormActive()
    {
        return FormsName::where('status_id','=',1)->select('uuid', 'name')->get();
    }

    public static function GetForms()
    {
        return FormsName::where('status_id','=',1)->orderBy('created_at', 'DESC')->get();
    }
}
