<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FormsType extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    public $fillable = ['id', 'uuid', 'name', 'users_id', 'status_id', 'created_at'];

    public static function GetTypeByName($name)
    {
        return FormsType::where([
            ['name', '=', $name],
            ['status_id', '=', '1']
            ])->first();
    }

    public static function GetForms()
    {
        return FormsType::orderBy('created_at', 'ASC')->get();
    }

    public static function GetFormsTable()
    {
        return FormsType::where('status_id', 1)
            ->join('data_status', 'forms_types.status_id', '=', 'data_status.number')
            ->select('forms_types.uuid', 'forms_types.name','forms_types.name as type', 'data_status.name as status')
            ->orderBy('forms_types.id', 'ASC')
            ->get();

    }

    public static function GetFormsById($id)
    {
        return DB::table('forms_types')
        ->join('data_status', 'forms_types.status_id', '=', 'data_status.number')
        ->where('forms_types.uuid', '=', $id)
        ->select('forms_types.uuid', 'forms_types.name','forms_types.name as type', 'data_status.name as status')
        ->get();

    }

    public static function GetFormById($id)
    {
        return FormsType::where('uuid', '=', $id)->first();
    }

}
