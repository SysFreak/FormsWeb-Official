<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class DataStatusEmail extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    public $fillable = ['id', 'uuid', 'form_id', 'next_id', 'status_id', 'created_at'];

    public static function GetBasicForm($id)
    {
        return DataStatusEmail::where('form_id', '=', $id)->first();
    }

    public static function GetFinishForm($id)
    {
        return DataStatusEmail::where([
            ['form_id', '=', $id],
            ['next_id', '=', '']
        ])->first();
    }

    public static function InsertBasicEmail($id)
    {
        $email              =   new DataStatusEmail();
        $email->uuid        =   Str::uuid()->toString();
        $email->form_id     =   $id;
        $email->next_id     =   '';
        $email->status_id   =   1;

        return ( $email->save() ) ? true : false;

    }

    public static function UpdateFinishEmail($id)
    {
        $email  =   DatastatusEmail::where('form_id', '=', $id['form_id'])->first();
        $email->next_id     =   $id['next_id'];

        return ( $email->save() ) ? true : false;
    }
}
