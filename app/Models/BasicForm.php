<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BasicForm extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    public $fillable = [    'id',           'uuid',         'name',             'last_name',
                    'cedula',       'age',          'email',            'phone_prin',
                    'phone_alt',    'state',        'city',             'postal',
                    'apartment',    'street',       'maps',             'reference',
                    'latitud',      'longitud',     'level',            'ceiling',
                    'ceiling',      'users_form',   'name_form',        'created_at'];

    public static function GetFormByName($id)
    {
        return BasicForm::where('name_form', '=', $id)->get();
    }

    public static function GetNameFormFinish($id)
    {
        return DB::table('basic_forms')->join('forms_names', 'basic_forms.name_form', '=', 'forms_names.uuid')->where('basic_forms.uuid', '=', $id)->select('forms_names.uuid','forms_names.name')->first();

        // return BasicForm::where('uuid', '=', $id)
        //         ->select('name_form')
        //         ->first();
    }


}

