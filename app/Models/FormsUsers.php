<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FormsUsers extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    public $fillable = ['id', 'uuid', 'username', 'email', 'name_id', 'type_id', 'status_id', 'created_at'];

    public static function GetUsersFormsId($id)
    {
        return FormsUsers::where([
            ['name_id',     '=', $id],
            ['status_id',   '=', '1']
        ])->get();
    }

    public static function GetUserId($id)
    {
        return FormsUsers::where([
            ['uuid',        '=', $id],
            ['status_id',   '=', '1']
        ])->first();
    }

    public static function GetForms()
    {
        return FormsUsers::orderBy('created_at', 'ASC')->get();
    }

    public static function GetFormsTable()
    {
        return DB::table('forms_users')->join('forms_names', 'forms_users.name_id', '=', 'forms_names.uuid')
                ->join('forms_types', 'forms_users.type_id', '=', 'forms_types.uuid')
                ->join('data_status', 'forms_users.status_id', '=', 'data_status.number')
                ->select('forms_users.uuid', 'forms_users.username', 'forms_users.email', 'forms_names.name as forms','forms_types.name as type', 'data_status.name as status')
                ->orderBy('forms_users.id', 'DESC')
                ->get();
    }

    public static function GetFormById($id)
    {
        return FormsUsers::where('uuid', '=', $id)->first();
    }
}
