<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FinishForm extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    use HasFactory;

    public $fillable = ['id', 'uuid', 'form_id', 'type_service', 'list_plan', 'name_com', 'rif_com', 'rep_com', 'email_com', 'provider', 'ups', 'created_at'];

    public static function GetFormById($id)
    {
        return FinishForm::where('form_id', '=', $id)->get();
    }

    public static function GetEmails()
    {
        return FinishForm::get();
    }
}
