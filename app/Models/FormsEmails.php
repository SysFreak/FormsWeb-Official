<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FormsEmails extends Model
{
    use HasFactory;

    public $fillable = [ 'id', 'uuid', 'email', 'email_type', 'status_id', 'form_id', 'form_name', 'created_at'];

    public static function GetEmailsByID($data)
    {

        $email =   [];

        $iData  =   FormsEmails::where([
            ['form_id',     '=', $data['id'] ],
            ['email_type',  '=', $data['type'] ],
            ['status_id',   '=', 1],
            ])->get();

        if(count($iData) >= "1")
        {
            return $iData;
        }else{
            return false;
        }

    }

    public static function GetFormById($id)
    {
        return FormsEmails::where('uuid', '=', $id)->first();
    }

    public static function GetForms()
    {
        return FormsEmails::orderBy('created_at', 'ASC')->get();
    }

    public static function GetFormsTable()
    {
        return DB::table('forms_emails')->join('forms_names', 'forms_emails.form_id', '=', 'forms_names.uuid')
                ->join('data_status', 'forms_emails.status_id', '=', 'data_status.number')
                ->select('forms_emails.id', 'forms_emails.uuid', 'forms_emails.email', 'forms_emails.email_type', 'forms_names.name as forms', 'data_status.name as status')
                ->orderBy('forms_emails.id', 'DESC')
                ->get();
    }

}
