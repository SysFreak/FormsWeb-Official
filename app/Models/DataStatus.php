<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DataStatus extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    public $fillable = ['id', 'uuid', 'name', 'number', 'created_at'];

    public static function GetByNum($id)
    {
        return DB::table('data_status')->where('number', '=', $id)->get();
    }

    public static function GetById($id)
    {
        return DB::table('data_status')->where('uuid', '=', $id)->get();
    }

    public static function GetAll()
    {
        return DB::table('data_status')->orderBy('number', 'ASC')->get();
    }

}
