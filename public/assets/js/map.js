let $comment = document.getElementById("maps");
let timeout;

var marker;
var coords = {};

initMap = function () {

    navigator.geolocation.getCurrentPosition(

    function (position)
    {
        coords = {
            lng: position.coords.longitude,
            lat: position.coords.latitude
        };
        document.getElementById("latitud").value = position.coords.latitude;
        document.getElementById("longitud").value = position.coords.longitude;
        setMapa(coords);
    },
    function (error)
    {
        console.log(error);
    });

}

function setMapa(coords)
{
    var map = new google.maps.Map(document.getElementById('map'),
    {
        zoom: 18,
        mapTypeId: 'hybrid',
        center: new google.maps.LatLng(coords.lat, coords.lng),
    });
    marker = new google.maps.Marker({
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        position: new google.maps.LatLng(coords.lat, coords.lng),
    });
    marker.addListener('click', toggleBounce);
    marker.addListener('dragend', function (event)
    {
        document.getElementById("latitud").value = this.getPosition().lat();
        document.getElementById("longitud").value = this.getPosition().lng();
    });
}

function toggleBounce()
{

    if (marker.getAnimation() !== null)
    {
        marker.setAnimation(null);
    } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
    }

}
