<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormsEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms_emails', function (Blueprint $table) {
            $table->id();
            $table->foreignUuid('uuid');
            $table->string('email');
            $table->integer('email_type')->default(1);
            $table->integer('status_id')->default(1);
            $table->foreignUuid('form_id');
            $table->string('form_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_emails');
    }
}
