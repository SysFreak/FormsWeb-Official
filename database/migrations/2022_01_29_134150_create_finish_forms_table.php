<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinishFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finish_forms', function (Blueprint $table) {
            $table->id();
            $table->foreignUuid('uuid');
            $table->foreignUuid('form_id');
            $table->integer('type_service')->default(1);
            $table->foreignUuid('list_plan');
            $table->string('name_com');
            $table->string('rif_com');
            $table->string('rep_com');
            $table->string('email_com');
            $table->string('provider');
            $table->string('ups');
            $table->string('covid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finish_forms');
    }
}
