<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBasicFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basic_forms', function (Blueprint $table) {
            $table->id();
            $table->foreignUuid('uuid');
            $table->string('name');
            $table->string('last_name');
            $table->string('cedula');
            $table->string('age');
            $table->string('email');
            $table->string('phone_prin');
            $table->string('phone_alt');
            $table->foreignUuid('state');
            $table->string('city');
            $table->string('postal');
            $table->string('apartment');
            $table->text('street');
            $table->text('maps');
            $table->text('reference');
            $table->string('latitud');
            $table->string('longitud');
            $table->foreignUuid('level');
            $table->foreignUuid('ceiling');
            $table->string('users_form');
            $table->foreignUuid('name_form');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basic_forms');
    }
}
