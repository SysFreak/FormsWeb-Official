<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms_users', function (Blueprint $table) {
            $table->id();
            $table->foreignUuid('uuid');
            $table->string('username');
            $table->string('email');
            $table->foreignUuid('name_id');
            $table->foreignUuid('type_id');
            $table->integer('status_id')->defaut(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms_users');
    }
}
