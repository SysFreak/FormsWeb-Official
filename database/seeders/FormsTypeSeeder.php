<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use Carbon\Carbon;

class FormsTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('forms_types')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(), 
            'name'          => "Sales", 
            'users_id'      => "0", 
            'status_id'     => "1", 
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'), 
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('forms_types')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(), 
            'name'          => "Campaign", 
            'users_id'      => "0", 
            'status_id'     => "1", 
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'), 
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('forms_types')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(), 
            'name'          => "External", 
            'users_id'      => "1", 
            'status_id'     => "1", 
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'), 
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);

    }
}
