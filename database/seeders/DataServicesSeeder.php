<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use Carbon\Carbon;

class DataServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('data_services')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Residencial", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_services')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Comercial", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
