<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use Carbon\Carbon;

class DataCeilingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('data_ceilings')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(),'name' => "Cemento", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
                DB::table('data_ceilings')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(),'name' => "Zinc", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
                DB::table('data_ceilings')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(),'name' => "Machiembrado", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
                DB::table('data_ceilings')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(),'name' => "Acerolit", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
                DB::table('data_ceilings')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(),'name' => "Mixto", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
                DB::table('data_ceilings')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(),'name' => "Otros", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
