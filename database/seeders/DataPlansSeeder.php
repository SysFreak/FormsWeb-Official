<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use Carbon\Carbon;

class DataPlansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "1 Mb Ilimitado - Promo Estudiantil - Primeros 12 Meses | $23.18",
            'type'          => "especial",
            'package'       => "residencial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "3 Mb Ilimitado - Promo Estudiantil - Primeros 12 Meses | $28.98",
            'type'          => "especial",
            'package'       => "residencial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "5 Mb Ilimitado - Promo Estudiantil - Primeros 12 Meses | $34.78",
            'type'          => "especial",
            'package'       => "residencial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Hasta 10Mb/10Mb de Velocidad Ilimitado | $46.38",
            'type'          => "inalambrico",
            'package'       => "residencial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Hasta 25Mb/25Mb de Velocidad Ilimitado | $57.98",
            'type'          => "inalambrico",
            'package'       => "residencial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Hasta 50Mb/50Mb de Velocidad Ilimitado | $69.58",
            'type'          => "inalambrico",
            'package'       => "residencial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Hasta 75Mb/75Mb de Velocidad Ilimitado | $92.78",
            'type'          => "inalambrico",
            'package'       => "residencial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Hasta 100Mb/100Mb de Velocidad Ilimitado | $115.98",
            'type'          => "inalambrico",
            'package'       => "residencial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Hasta 50Mb/50Mb de Velocidad Ilimitado | $34.78", 
            'type'          => "fibra",
            'package'       => "residencial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Hasta 10Mb/10Mb de Velocidad Ilimitado | $46.38", 
            'type'          => "inalambrico",
            'package'       => "comercial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Hasta 25Mb/25Mb de Velocidad Ilimitado | $57.98", 
            'type'          => "inalambrico",
            'package'       => "comercial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Hasta 50Mb/50Mb de Velocidad Ilimitado | $69.58", 
            'type'          => "inalambrico",
            'package'       => "comercial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        
        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Hasta 75Mb/75Mb de Velocidad Ilimitado | $92.78", 
            'type'          => "inalambrico",
            'package'       => "comercial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Hasta 100Mb/100Mb de Velocidad Ilimitado | $115.98", 
            'type'          => "inalambrico",
            'package'       => "comercial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Hasta 50Mb/50Mb de Velocidad Ilimitado | $46.38", 
            'type'          => "fibra",
            'package'       => "comercial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Hasta 100Mb/100Mb de Velocidad Ilimitado | $57.98", 
            'type'          => "fibra",
            'package'       => "comercial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Hasta 250Mb/250Mb de Velocidad Ilimitado | $115.98", 
            'type'          => "fibra",
            'package'       => "comercial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Hasta 500Mb/500Mb de Velocidad Ilimitado | $144.98", 
            'type'          => "fibra",
            'package'       => "comercial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('data_plans')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Hasta 1000Mb/1000Mb de Velocidad Ilimitado | $173.98", 
            'type'          => "fibra",
            'package'       => "comercial",
            'status_id'        => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
