<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use Carbon\Carbon;

class FormsUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('forms_users')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(), 
            'username'      => "Lcampos", 
            'email'         => "luis.924@boomsolutions.com", 
            'name_id'       => "a9769d4e-b264-4e9b-8bb6-41374d35fb2b",
            'type_id'       => "be7a60fb-a301-475c-854e-4b08c4aafc3d",
            'status_id'     => "1", 
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'), 
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('forms_users')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(), 
            'username'      => "Yeferson.Mkt", 
            'email'         => "yeferson.marketing@boomsolutions.com", 
            'name_id'       => "a9769d4e-b264-4e9b-8bb6-41374d35fb2b",
            'type_id'       => "be7a60fb-a301-475c-854e-4b08c4aafc3d",
            'status_id'     => "1", 
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'), 
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
