<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Models\FormsType;
use App\Models\FormsUsers;

use Carbon\Carbon;

class FormsNameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('forms_names')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(), 
            'name'          => "solicitud", 
            'password'      => Str::random(40),
            'type_id'       => FormsType::GetTypeByName('Sales')->uuid,
            'status_id'     => "1", 
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'), 
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('forms_names')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(), 
            'name'          => "campaign", 
            'password'      => Str::random(40),
            'type_id'       => FormsType::GetTypeByName('Campaign')->uuid, 
            'status_id'     => "1", 
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'), 
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('forms_names')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(), 
            'name'          => "external", 
            'password'      => Str::random(40),
            'type_id'       => FormsType::GetTypeByName('External')->uuid, 
            'status_id'     => "1", 
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'), 
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
