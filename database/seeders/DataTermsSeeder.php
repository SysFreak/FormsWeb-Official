<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use Carbon\Carbon;

class DataTermsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('data_terms')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "El usuario solamente comenzar&aacute; a pagar el servicio, una vez se haya completado de manera satisfactoria la instalaci&oacute;n.", 
            'package'       => "residencial",
            'status_id'     => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_terms')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Activaci&oacute;n &uacute;nica de <b>$199.99 + Prorrateo + IVA</b> (Incluye instalaci&oacute;n t&eacute;cnica)", 
            'package'       => "residencial",
            'status_id'     => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_terms')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Acuerdo comercial de <b>24 meses</b> con atenci&oacute;n al cliente los 7 d&iacute;as de la semana.", 
            'package'       => "residencial",
            'status_id'     => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_terms')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "El servicio te incluye equipos como <b>antena, modem router, cableado, UPS, ONU, Wallplate</b> (depende del tipo de instalaci&oacute;n).", 
            'package'       => "residencial",
            'status_id'     => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_terms')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "<b>Los equipos incluidos son propiedad de la empresa</b>, la cual ser&aacute; la responsable de su sustituci&oacute;n o reemplazo en caso de desperfectos no asociados a mala manipulaci&oacute;n.", 
            'package'       => "residencial",
            'status_id'     => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_terms')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "<b>Pagos de mensualidades</b> con transferencia Zelle o Efectivo en divisa que <b>deben ser realizados hastalos d&iacute;as 5 de cada mes.</b>", 
            'package'       => "residencial",
            'status_id'     => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_terms')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "El usuario solamente comenzar&aacute; a pagar el servicio, una vez se haya completado de manera satisfactoria la instalaci&oacute;n.", 
            'package'       => "comercial",
            'status_id'     => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_terms')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Activaci&oacute;n &uacute;nica de <b>$249.99 + Prorrateo + IVA</b> (Incluye instalaci&oacute;n t&eacute;cnica).", 
            'package'       => "comercial",
            'status_id'     => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_terms')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Acuerdo comercial de <b>36 meses</b> con atenci&oacute;n al cliente los 7 d&iacute;as de la semana.", 
            'package'       => "comercial",
            'status_id'     => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_terms')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "El servicio te incluye equipos como <b>antena, modem router, cableado, UPS, ONU, Wallplate</b> (depende del tipo de instalaci&oacute;n).", 
            'package'       => "comercial",
            'status_id'     => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_terms')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "<b>Los equipos incluidos son propiedad de la empresa</b>, la cual ser&aacute; la responsable de su sustituci&oacute;n o reemplazo en caso de desperfectos no asociados a mala manipulaci&oacute;n.", 
            'package'       => "comercial",
            'status_id'     => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_terms')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "<b>Pagos de mensualidades</b> con transferencia Zelle o Efectivo en divisa que <b>deben ser realizados hasta los d&iacute;as 5 de cada mes.</b>", 
            'package'       => "comercial",
            'status_id'     => "1",
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);        
    }
}
