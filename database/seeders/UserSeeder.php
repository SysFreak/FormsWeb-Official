<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\FormsName;

use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('users')->insert([
        //     'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
        //     'name'          => "Solicitud",
        //     'username'      => "solicitud",
        //     'email'         => "solicitud@boomsolutions.com",
        //     'password'      => bcrypt(FormsName::GetUsersForms('solicitud')[0]->password),
        //     'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
        //     'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        // ]);
        // DB::table('users')->insert([
        //     'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
        //     'name'          => "Campaign",
        //     'username'      => "campaign",
        //     'email'         => "campaign@boomsolutions.com",
        //     'password'      => bcrypt(FormsName::GetUsersForms('campaign')[0]->password),
        //     'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
        //     'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        // ]);
        // DB::table('users')->insert([
        //     'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
        //     'name'          => "External",
        //     'username'      => "external",
        //     'email'         => "external@boomsolutions.com",
        //     'password'      => bcrypt(FormsName::GetUsersForms('external')[0]->password),
        //     'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
        //     'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        // ]);
        DB::table('users')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Luis Campos",
            'username'      => "lcampos",
            'email'         => "luis.924@boomsolutions.com",
            'password'      => bcrypt('nd4spd'),
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'name'          => "Yeferson Morales",
            'username'      => "yeferson.marketing",
            'email'         => "yeferson.marketing@boomsolutions.com",
            'password'      => bcrypt('Boom1234'),
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
