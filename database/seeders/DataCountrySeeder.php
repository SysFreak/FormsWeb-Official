<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use Carbon\Carbon;

class DataCountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Amazonas", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Anzoategui", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Apure", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Aragua", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Barinas", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Bolivar", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Carabobo", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Cojedes", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Delta Amacuro", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Distrito Capital", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Falcon", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Guarico", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Lara", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') 
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Merida", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Miranda", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Monagas", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Nueva Esparta", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Portuguesa", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Sucre", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Tachira", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Trujillo", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Vargas", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Yaracuy", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('data_countries')->insert([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()->toString(), 'name' => "Zulia", 'status_id' => "1", 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
