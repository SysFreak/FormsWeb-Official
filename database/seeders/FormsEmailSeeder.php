<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

Use App\Models\FormsName;

use Carbon\Carbon;

class FormsEmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Forms   =   FormsName::where('status_id', '=', '1')->get();

        foreach ($Forms as $f => $form) 
        {
            DB::table('forms_emails')->insert([
                'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(), 
                'email'         => "luis.924@boomsolutions.com", 
                'email_type'    => '1',
                'status_id'     => "1", 
                'form_id'       => $form->uuid,
                'form_name'     => $form->name,
                'created_at'    => Carbon::now()->format('Y-m-d H:i:s'), 
                'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ]);

            DB::table('forms_emails')->insert([
                'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(), 
                'email'         => "diosne.marketing@boomsolutions.com", 
                'email_type'    => '2',
                'status_id'     => "1", 
                'form_id'       => $form->uuid,
                'form_name'     => $form->name,
                'created_at'    => Carbon::now()->format('Y-m-d H:i:s'), 
                'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ]);
            
            DB::table('forms_emails')->insert([
                'uuid'          => \Ramsey\Uuid\Uuid::uuid4()->toString(), 
                'email'         => "andreap.marketing@boomsolutions.com", 
                'email_type'    => '2',
                'status_id'     => "1", 
                'form_id'       => $form->uuid,
                'form_name'     => $form->name,
                'created_at'    => Carbon::now()->format('Y-m-d H:i:s'), 
                'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }
    }
}
