<?php

use App\Http\Controllers\FormsController;
use App\Http\Controllers\PanelController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\FacturacionController;
use App\Http\Controllers\MarketingController;
use App\Http\Controllers\MassiveLeadsController;
use Illuminate\Support\Facades\Route;

use App\Models\FormsName;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return redirect('/solicitud'); });

$forms      =   FormsName::where('status_id', '=', '1')->select('name')->get();

if(COUNT($forms) > 0)
{
   foreach ($forms as $f => $form)
   {
       Route::get(''.$form->name.'',  [FormsController::class, 'solicitud']);
   }
}

Route::post('/forms/curl', [FormsController::class, 'formscurl']);

Route::get('/email/marketing',  [MarketingController::class,'EmailMarketing']);
Route::get('/email/track/{id}',  [MarketingController::class,'TrackMarketing']);

Route::group(['middleware' => 'auth.jwt'], function()
{
    Route::post('/load',        [FormsController::class, 'load']);
    Route::post('/basicforms',  [FormsController::class, 'basicforms']);
    Route::post('/finishforms', [FormsController::class, 'finishforms']);
});

Route::get('/cpanel',               [PanelController::class, 'view'])->name('/cpanel');
Route::post('/cpanel/dashboard',    [PanelController::class, 'dashboard'])->name('/cpanel/dashboard');

Route::group(['middleware' => 'auth.jwt'], function()
{
    Route::get('/cpanel/dashboard',                 [PanelController::class, 'dashboard'])->name('/cpanel/dashboard');
    Route::get('/cpanel/forms',                     [PanelController::class, 'forms'])->name('/cpanel/forms');
    Route::post('/cpanel/formsEdit',                [PanelController::class, 'formsEdit'])->name('/cpanel/formsEdit');
    Route::post('/cpanel/formsRegister',            [PanelController::class, 'formsRegister'])->name('/cpanel/formsRegister');
    Route::put('/cpanel/formsUpdate',               [PanelController::class, 'formsUpdate'])->name('/cpanel/formsUpdate');

    Route::post('/cpanel/formsTypesEdit',           [PanelController::class, 'formsTypesEdit'])->name('/cpanel/formsTypesEdit');
    Route::post('/cpanel/formsTypesRegister',       [PanelController::class, 'formsTypesRegister'])->name('/cpanel/formsTypesRegister');
    Route::put('/cpanel/formsTypesUpdate',          [PanelController::class, 'formsTypesUpdate'])->name('/cpanel/formsTypesUpdate');

    Route::post('/cpanel/formsUsersEdit',           [PanelController::class, 'formsUsersEdit'])->name('/cpanel/formsUsersEdit');
    Route::post('/cpanel/formsUsersRegister',       [PanelController::class, 'formsUsersRegister'])->name('cl/cpanel/formsUsersRegister');
    Route::put('/cpanel/formsUsersUpdate',          [PanelController::class, 'formsUsersUpdate'])->name('/cpanel/formsUsersUpdate');

    Route::post('/cpanel/formsEmailsEdit',          [PanelController::class, 'formsEmailsEdit'])->name('/cpanel/formsEmailsEdit');
    Route::post('/cpanel/formsEmailsRegister',      [PanelController::class, 'formsEmailsRegister'])->name('/cpanel/formsEmailsRegister');
    Route::put('/cpanel/formsEmailsUpdate',         [PanelController::class, 'formsEmailsUpdate'])->name('/cpanel/formsEmailsUpdate');

    Route::get('/cpanel/types',                     [PanelController::class, 'types'])->name('/cpanel/types');

    
});

Route::get('/cpanel/massive',                       [MassiveLeadsController::class, 'index'])->name('/cpanel/massive');
Route::post('/cpanel/massive/upload',               [MassiveLeadsController::class, 'upload'])->name('/cpanel/massive/upload');
Route::get('/cpanel/massive/load',                  [MassiveLeadsController::class, 'load'])->name('/cpanel/massive/load');
Route::post('/cpanel/massive/process',              [MassiveLeadsController::class, 'process'])->name('/cpanel/massive/process');

Route::get('/test',                                 [TestController::class, 'index']);

Route::get('/facturacion',                          [FacturacionController::class, 'index'])->name('/facturacion');
Route::post('/facturacion/api',                     [FacturacionController::class, 'api'])->name('/facturacion/api');
Route::post('/facturacion/api/forms',               [FacturacionController::class, 'forms'])->name('/facturacion/api/forms');