<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Boom Solutions</title>
    <link rel="icon" href="https://boomsolutionsve.com/wp-content/uploads/2020/05/cropped-favicon-32x32.png" sizes="32x32" />
</head>

<body bgcolor="#FFFFFF">

    @yield('content')

</body>

</html>
