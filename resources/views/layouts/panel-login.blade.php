<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	@include('includes.panel-login-head')
</head>

<body class="hold-transition login-page">

	<div class="login-box">


		@yield('content')
		
	</div>	

	@include('includes.panel-login-footer')

	@yield('js')

</body>

</html>
