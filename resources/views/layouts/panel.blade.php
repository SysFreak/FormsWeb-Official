<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	@include('includes.panel-head')
</head>

<body class="sidebar-mini layout-navbar-fixed sidebar-collapse">

	<div class="wrapper">

		@yield('content')
		
	</div>	

	@include('includes.panel-footer')

	@yield('js')

</body>

</html>
