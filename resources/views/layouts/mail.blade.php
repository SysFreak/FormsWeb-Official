<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	@include('includes.mails-head')
</head>

<body bgcolor="#FFFFFF">

    <table class="head-wrap" bgcolor="#002260">
        <tr>
            <td class="header container">
                <div class="content">
                    <table bgcolor="#002260">
                        <tr style="text-align: center; width: 75px;">
                            <td><img src="https://i.ibb.co/Mg8JtcR/LOGOTIPOS-BOOM-SOLUTIONS-blanco-1.png" style="width: 100px;"/></td>
                        </tr>
                    </table>
                </div>          
            </td>
        </tr>
    </table>


    @yield('content')
			
	@yield('js')

</body>

</html>
