<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	@include('includes.forms-head')
    {{-- <style>
        .map{
            margin: 0 auto;
            width: 300px;
            height: 300px;
        }
    </style> --}}
</head>

<body>

	<div class="card">

		@yield('content')

	</div>

	@include('includes.forms-footer')

	@yield('js')

</body>

</html>
