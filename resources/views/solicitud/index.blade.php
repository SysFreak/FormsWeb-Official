@extends('layouts.forms')

@section('content')
    <div class="card">

        <div class="card-header">
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container">
                    <a class="navbar-brand" href="https://boomsolutionsve.com/">
                        <img class="w-25" src="{{ asset('assets/img/logocolor.png') }}" alt="Logo de Boom Solutions">
                    </a>
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                        <a class="nav-link" href="https://boomsolutionsve.com/">Inicio <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="https://boomsolutionsve.com/#nosotros">Nosotros</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="https://boomsolutionsve.com/#precios">Precios</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="https://boomsolutionsve.com/#contacto">Contáctanos</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="http://boomsolutions.speedtestcustom.com/">Speedtest</a>
                        </li>
                        <li class="nav-item text-nowrap">
                        <a class="nav-link btn-naranja text-white rounded"
                            href="https://facturacion.boomsolutionsve.com/">Registrar pago</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

        <h1 class="font-weight-bold mt-5 text-center" style="color: #001c62;">
            SOLICITUD DE SERVICIO DE INTERNET
        </h1>
        <h5 class="font-weight-bold text-center" style="color: #001c62;">
            Completa la solicitud para que obtengas la visita de instalaci&oacute;n con la mayor prioridad
        </h5>

        <div class="card-body w-75 align-self-center mt-5 bg-light">
        @csrf
            <div id="formBasic">
                <h2 class="font-weight-bold text-center" style="color: #FF6600;">
                    Paso 1/2
                </h2>
                <form name="BasicForm" id="BasicForm" method="post" action="javascript:void(0)">
                    <input type="text" class="form-control" id="name_form" name="name_form" value="{{ $form }}" hidden>
                    <input type="text" class="form-control" id="pass_form" name="pass_form" value="{{ $pass }}" hidden>
                    <input type="text" class="form-control" id="username_form" name="username_form" value="{{ $username }}" hidden>

                    <div class="form-group">

                        <h4 class="font-weight-bold text-center" style="color: #001c62;">
                            Datos b&aacute;sicos
                        </h4>

                        @if( $users !== null)
                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <label for="name">Operadores</label>
                                    <select class="custom-select d-block w-100" id="users_form" name="users_form" >
                                        @foreach($users as $user)
                                            <option value="{{ $user->uuid }}">{{ $user->email }}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                        @endif

                        <div class="row mt-3">
                            <div class="col-md-6">
                                <label for="name">Nombres del titular</label>
                                <input type="text" class="form-control restricted" id="name" name="name" placeholder="Nombres del titular" onkeypress="return restricted(event)">
                            </div>
                            <div class="col-md-6">
                                <label for="last_name">Apellidos del titular</label>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Apellidos del titular" onkeypress="return restricted(event)">
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-6">
                                <label for="cedula">C&eacute;dula de identidad</label>
                                <input type="text" class="form-control" id="cedula" name="cedula" placeholder="C&eacute;dula con el patr&oacute;n 12345678 &oacute; E-12345678">
                            </div>
                            <div class="col-md-6">
                                <label for="age">Fecha de nacimiento</label>
                                <input type="text" class="form-control" id="age" name="age" placeholder="Ej. 01/01/2000">
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-12">
                                <label for="email">Correo electr&oacute;nico</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Ej. user@gmail.com">
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-6">
                                <label for="phone_prin">N&uacute;mero de tel&eacute;fono principal</label>
                                <input type="number" class="form-control" id="phone_prin" name="phone_prin" placeholder="Ej. 4141234567">
                            </div>
                            <div class="col-md-6">
                            <label for="phone_alt">N&uacute;mero de tel&eacute;fono alternativo</label>
                                <input type="number" class="form-control" id="phone_alt" name="phone_alt" placeholder="Ej. 4141234567">
                            </div>
                        </div>

                        <br>

                        <h4 class="font-weight-bold text-center" style="color: #001c62;">
                            Datos de la direcci&oacute;n
                        </h4>

                        <div class="row mt-3">
                            <div class="col-md-6">
                                <label for="state">Selecciona el estado en el que deseas instalar el servicio</label>
                                <select class="custom-select d-block w-100" id="state" name="state" >
                                    <option value="">Listado de estados</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="city">Escriba la ciudad en la que desea instalar el servicio</label>
                                <input type="text" class="form-control" id="city" name="city" placeholder="Ejemplo: Barquisimeto" onkeypress="return restricted(event)">
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-6">
                                <label for="postal">Escriba el <b>c&oacute;digo postal</b></label>
                                <input type="number" class="form-control" id="postal" name="postal" placeholder="Ej. 3001">
                            </div>
                            <div class="col-md-6">
                            <label for="apartment">Escriba la direcci&oacute;n exacta donde desea instalar el servicio</b></label>
                                <input type="text" class="form-control" id="address" name="address" placeholder="Ej: Carrera 19 con calle 42 Barquisimeto">
                                <input type="text" class="form-control" id="apartment" name="apartment" value='' placeholder="N&uacute;mero de casa o apartamento" readonly hidden>
                            </div>
                        </div>

                        <div class="row mt-3" hidden>
                            <div class="col-md-6">
                                <label for="street">Escribe la calle, avenida o urbanizaci&oacute;n donde deseas instalar el servicio</label>
                                <input type="text" class="form-control" id="street" name="street" value="" placeholder="Calle, avenida o nombre de edificio" readonly>
                            </div>
                            <div class="col-md-6">
                                <label for="maps">Escribe el <b>barrio o sector</b> en la que deseas instalar el servicio</label>
                                <input type="text" class="form-control" id="maps" name="maps" value='' placeholder="Urbanizaci&oacute;n, barrio o sector" readonly>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-12">
                                <label for="reference">Por favor, ind&iacute;canos un punto o lugar de referencia, lo m&aacute;s detallado posible.</b> </label>
                                <input type="text" class="form-control" id="reference" name="reference" placeholder="Punto o lugar de referencia" >
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div style="color: #001c62;">
                                <h4 class=" d-flex justify-content-center special-heading text-center mt-5 mb-4">
                                    Adicionalmente, ind&iacute;canos en el mapa la ubicaci&oacute;n en la que desea instalar el servicio para una consulta de cobertura
                                </h4>
                                <ol>
                                    <li class="text-left">
                                        <strong>Escoge tu direcci&oacute;n de manera manual arrastrando el icono rojo al lugar donde desea instalar el servicio.</strong>
                                    </li>
                                    <li class="text-left"
                                        ><b>Mantén presionado el icono y arr&aacute;stralo hasta posicionarlo en la ubicaci&oacute;n exacta donde desea instalar el serivio.</b>
                                    </li>
                                    <li class="text-left">
                                        <strong>Luego que encuentres la ubicaci&oacute;n, completa el resto del formulario.</strong>
                                    </li>
                                </ol>
                                <p class="text-center"><span class="text-muted">*Las coordenadas se agregan autom&aacute;ticamente al formulario.</span></p>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div id="map"></div>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-6">
                                <label for="latitud">Coordenadas, latitud</label>
                                <input type="text" class="form-control" id="latitud" name="latitud" placeholder="Coordandas Latitud" value="0.0000">
                            </div>
                            <div class="col-md-6">
                                <label for="longitud">Coordenadas, longitud</label>
                                <input type="text" class="form-control" id="longitud" name="longitud" placeholder="Coordandas Longitud" value="0.0000">
                            </div>
                        </div>

                        <br>

                        <h4 class="font-weight-bold text-center" style="color: #001c62;">
                            Datos de la vivienda
                        </h4>

                        <div class="row mt-3">
                            <div class="col-md-6">
                                <label for="level">Nivel de la edificaci&oacute;n</label>
                                <select class="custom-select d-block w-100" id="level" name="level" >
                                    <option value="">Seleccione un nivel</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                            <label for="ceiling">Tipo de techo</label>
                                <select class="custom-select d-block w-100" id="ceiling" name="ceiling" >
                                <option value="">Listado de tipo de techo</option>
                                </select>
                            </div>
                        </div>

                        <br>

                        <h4 class="font-weight-bold text-center" style="color: #001c62;" >
                            T&eacute;rminos y condiciones del servicio.
                        </h4>
                        
                        <h5 class="font-weight-bold text-center" style="color: #001c62; font-size: 10px;">
                            * Marca la casillas para indicar si estas de acuerdo con los t&eacute;minos y condiciones.
                        </h5>

                        <div class="row mt-3">
                            <div class="col-md-12">
                                <ul class="text-left">
                                    <li>Declaro que toda la informaci&oacute;n proporcionada es verdadera, completa, correcta, y puede ser verificada. </li>
                                </ul>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-12" style="text-align: center;">
                                <input class="custom-checkbox text-center" type="checkbox" name="term_data" id="term_data" value="accepted"  />
                                <label for="terms_data"><b>Le&iacute; los t&eacute;rminos y condiciones</b></label>
                            </div>
                        </div>

                        <div class="row">

                            <button type="submit" class="btn mt-4 btn-naranja btn-lg btn-block" id="siguiente" onclick="NextForms();">Siguiente</button>

                        </div>

                    </div>


                </form>
            </div>
            <div id="formNext" hidden>
                <form name="NextForm" id="NextForm" method="post" action="javascript:void(1)">
                    <div class="form-group">
                        <h2 class="font-weight-bold text-center" style="color: #FF6600;">
                            Paso 2/2
                        </h2>
                    </div>
                    <input type="text" class="form-control" id="form_id" name="form_id" value="{{ $form }}" hidden>
                    <div class="form-group">
                        <h4 class="font-weight-bold text-center" style="color: #001c62;">
                            Datos del servicio
                        </h4>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <label for="state">Selecciona un tipo de servicio</label>
                            <select class="custom-select d-block w-100" id="type_service" name="type_service" >
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="state">Selecciona el tipo de plan que desea activar</label>
                            <select class="custom-select d-block w-100" id="list_plan" name="list_plan" >
                            </select>
                        </div>
                    </div>

                    <div class="p-2 font-italic">*Velocidades, tipo de conexi&oacute;n y cobertura disponibles de acuerdo a la ubicaci&oacute;n. Precios incluyen IVA</div>

                    <br>

                    <div id="ComercialData" hidden>
                        <h4 class="font-weight-bold text-center" style="color: #001c62;">
                            Datos de cliente comercial
                        </h4>
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <label for="name_com">Nombre de la Compa&ntilde;ia o Comercio</label>
                                <input type="text" class="form-control" id="name_com" name="name_com" placeholder="Nombre del Comercio">
                            </div>
                            <div class="col-md-6">
                                <label for="rif_com">Rif</label>
                                <input type="text" class="form-control" id="rif_com" name="rif_com" placeholder="Coloca el RIF con el patr&oacute;n J-12345678-1">
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <label for="rep_com">Representante Legal</label>
                                <input type="text" class="form-control" id="rep_com" name="rep_com" placeholder="Representante Legal">
                            </div>
                            <div class="col-md-6">
                                <label for="email_com">Correo electr&oacute;nico</label>
                                <input type="email" class="form-control" id="email_com" name="email_com" placeholder="Ej. representante@gmail.com">
                            </div>
                        </div>
                    </div>

                    <br>

                    <h4 class="font-weight-bold text-center" style="color: #001c62;">
                        Informaci&oacute;n adicional
                    </h4>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <label for="provider">¿Cu&aacute;l es tu proveedor actual de internet?</label>
                            <input type="text" class="form-control" id="provider" name="provider" placeholder="Cantv, Inter, Movistar...">
                        </div>
                        <div class="col-md-6">
                            <label for="rif_com">¿Desea adquirir el UPS como respaldo el&eacute;ctrico ? </label>
                            <select class="custom-select d-block w-100" id="ups" name="ups" >
                                <option value="Si">Si</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <label for="provider">Para culminar la solicitud y por motivos de seguridad d&eacute;janos saber si en las &uacute;ltimas semanas, ¿han presentado alg&uacute;n s&iacute;ntoma relacionado con el COVID-19?</label>
                            <select class="custom-select d-block w-100" id="covid" name="covid" >
                                <option value="No">No</option>
                                <option value="Si">Si</option>
                            </select>
                        </div>
                    </div>

                    <br>

                    <h4 class="font-weight-bold text-center" style="color: #001c62;">
                        T&eacute;rminos y condiciones.
                    </h4>
                    <h5 class="font-weight-bold text-center" style="color: #001c62; font-size: 10px;">
                        * Marca las casillas para indicar si estas de acuerdo con los t&eacute;minos y condiciones.
                    </h5>


                    <div id="termsConds">
                        <div class="row mt-3">
                            <ul class="text-left" style="font-size: 12px;">
                                <div id="termsCondsTable">
                                </div>
                            </ul>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12" style="text-align: center;">
                                <div id="termsCondsCheck">
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">

                        <button type="submit" class="btn mt-4 btn-naranja btn-lg btn-block" id="finish" onclick="FinishForms();">Finalizar</button>

                    </div>
                </form>
            </div>

        </div>

    </div>
@endsection

@section('js')
    @include('solicitud.js')
@endsection
