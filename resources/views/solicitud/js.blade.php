<script>

blockUI();

localStorage.clear();



$.ajax({
    url: "{{ url('/api/login') }}",
    type: "post",
    data: { username: $('#username_form').val(), password: $('#pass_form').val(), form: $('#name_form').val() },
    headers:{
        'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
        'Content-Type': 	'application/x-www-form-urlencoded'
    },
    success: function(data, textStatus, xhr)
    {
        if(xhr.status == 200)
        {
            localStorage.clear();
            localStorage.setItem("token", data.token);
        }
    },
    error: function( xhr, ajaxOptions, thrownError )
    {
        ErrorToken(JSON.parse(xhr.responseText).error);
    }
});

setTimeout(
    function()
    {
        $.ajax({
            url: "{{ url('/load') }}",
            type: "post",
            data: { username: $('#username_form').val(), password: $('#pass_form').val(), form: $('#name_form').val() },
            headers:{
                'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
                'Content-Type': 	'application/x-www-form-urlencoded',
                'Authorization':	"bearer " + localStorage.getItem("token")
            },
            success: function(data, textStatus, xhr)
            {
                Terms(data.terms, '1');
                Plans(data.plans, '1');
                Services(data.services);
                Options(data.ceilings, 'ceiling');
                Options(data.levels, 'level');
                Options(data.country, 'state');
                $.unblockUI();
            },
            error: function( xhr, ajaxOptions, thrownError )
            {
                ErrorToken(JSON.parse(xhr.responseText).error);
            }
        });
    },
1000);

function NextForms()
{
    var terms   =   CheckTerms();

    $("#BasicForm").find(':input').each(function()
    {
        var elemento = this;
        var final = elemento.id+'-error';
        $('head').append('<style>#'+final+'{ color: red; } </style>');
    });

    if(terms)
    {
        $('#BasicForm').validate({
            rules:{
                name:		{ required: true, minlength: 2,  maxlength: 50},
                last_name:	{ required: true, minlength: 2,  maxlength: 50},
                cedula:		{ required: true, minlength: 1,  maxlength: 12},
                age:		{ required: true, minlength: 10, maxlength: 10},
                email:		{ required: true, minlength: 10, maxlength: 50, email: true },
                phone_prin:	{ required: true, minlength: 10, maxlength: 14},
                phone_alt:	{ required: true, minlength: 10, maxlength: 14},
                city:		{ required: true, minlength: 2,  maxlength: 100},
                state:		{ required: true },
                postal:		{ required: true, minlength: 2,  maxlength: 10},
                address:	{ required: true, minlength: 1,  maxlength: 250},
                reference:	{ required: true, minlength: 2,  maxlength: 254},
                latitud:	{ required: true, minlength: 2,  maxlength: 50},
                longitud:	{ required: true, minlength: 2,  maxlength: 50},
                level:		{ required: true },
                ceiling:	{ required: true },
            },
            messages:{
                TipServ:{
                    required: "Por favor seleccione una opcion valida"
                },
                name:	{
                    required: "Por favor introduzca un nombre v&aacute;lido", minlength: "Su nombre debe contener mas de 2 caracteres", maxlength: "Su nombre no debe exceder los 50 caracteres"},
                last_name:	{
                    required: "Por favor introduzca un apellido v&aacute;lido", minlength: "Su apellido debe contener mas de 2 caracteres", maxlength: "Su apellido no debe exceder los 50 caracteres"},
                cedula:	{
                    required: "Por favor introduzca una c&eacute;dula", minlength: "Su c&eacute;dula debe contener mas de 1 caracteres", maxlength: "Su c&eacute;dula no debe exceder los 12 caracteres"},
                age:	{
                    required: "Por favor introduzca una fecha v&aacute;lida", minlength: "Su fecha debe contener 10 caracteres", maxlength: "Su fecha no debe exceder los 10 caracteres"},
                email:	{
                    required: "Por favor introduzca un email v&aacute;lido", minlength: "Su email debe contener mas de 10 caracteres", maxlength: "Su email no debe exceder los 100 caracteres"},
                phone_prin:	{
                    required: "Por favor introduzca un tel&eacute;fono v&aacute;lido", minlength: "Su tel&eacute;fono debe contener 10 caracteres", maxlength: "Su tel&eacute;fono no debe exceder los 10 caracteres"},
                phone_alt:	{
                    required: "Por favor introduzca un tel&eacute;fono v&aacute;lido", minlength: "Su tel&eacute;fono debe contener 10 caracteres", maxlength: "Su tel&eacute;fono no debe exceder los 10 caracteres"},
                city:	{
                    required: "Por favor introduzca una ciudad v&aacute;lida", minlength: "Su ciudad debe contener mas de 2 caracteres", maxlength: "Su ciudad no debe exceder los 50 caracteres"},
                state:	{
                    required: "Por favor seleccione un estado v&aacute;lida"},
                postal:	{
                    required: "Por favor introduzca un c&oacute;digo postal v&aacute;lida", minlength: "Su c&oacute;digo postal debe contener mas de 2 caracteres", maxlength: "Su c&oacute;digo postal no debe exceder los 10 caracteres"},
                address:	{
                    required: "Por favor introduzca una direcci&oacute;n v&aacute;lida", minlength: "Su direcci&oacute;n debe contener mas de 2 caracteres", maxlength: "Su direcci&oacute;n no debe exceder los 250 caracteres"},
                reference:	{
                    required: "Por favor introduzca un punto de referencia v&aacute;lida", minlength: "Su punto de referencia debe contener mas de 2 caracteres", maxlength: "Su punto de referencia no debe exceder los 254 caracteres"},
                latitud:	{
                    required: "Por favor introduzca una latitud v&aacute;lida", minlength: "Su latitud debe contener mas de 2 caracteres", maxlength: "Su latitud no debe exceder los 50 caracteres"},
                longitud:	{
                    required: "Por favor introduzca una longitud v&aacute;lida", minlength: "Su longitud debe contener mas de 2 caracteres", maxlength: "Su longitud no debe exceder los 50 caracteres"},
                level:	{
                    required: "Por favor seleccione un nivel v&aacute;lida"},
                ceiling:	{
                    required: "Por favor seleccione un techo v&aacute;lida"}
                },
            submitHandler: function(form)
            {
                $('#siguiente').html('Please Wait...');
                $('#siguiente').attr("disabled", true);
                blockUI();
                $.ajax({
                    url: "{{ url('basicforms') }}",
                    type: "POST",
                    data: $('#BasicForm').serialize(),
                    headers:{
                        'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
                        'Content-Type': 	'application/x-www-form-urlencoded',
                        'Authorization':	"bearer " + localStorage.getItem("token")
                    },
                    success: function(data, textStatus, xhr)
                    {
                        if(xhr.status == 200)
                        {
                            $('#BasicForm')[0].reset();
                            if(data.next ==  true)
                            {
                                $('#form_id').val(data.id);
                                $('#siguiente').html('Siguiente');
                                $('#siguiente').attr("disabled", false);
                                $('#formBasic').attr("hidden", true);
                                $('#formNext').attr("hidden", false);
                                $.unblockUI();
                            }else{
                                $('#BasicForm')[0].reset();
                                $('#NextForm')[0].reset();
                                $('#formBasic').attr("hidden", false);
                                $('#formNext').attr("hidden", true);
                                $('#siguiente').html('Siguiente');
                                $('#siguiente').attr("disabled", false);
                                event.preventDefault();
                                SuccessMessage('Country')
                                return false;
                                $.unblockUI();
                            }

                            $('#siguiente').html('siguiente');
                            $('#siguiente').attr("disabled", true);
                        }
                    },
                    error: function( xhr, ajaxOptions, thrownError )
                    {
                        // localStorage.clear();
                        // localStorage.setItem("error", JSON.parse(xhr.responseText).error);
                        ErrorToken(JSON.parse(xhr.responseText).error);
                        $.unblockUI();
                    }
                });
            }
        })
    }else{
        Swal.fire(
            'T&eacute;rminos y condiciones!',
            'Lee detenidamente y acepta los t&eacute;rminos y condiciones del servicio para completar la solicitud',
            'error'
        );
    }
}

$('#type_service').change(function()
{
    let sType   =   $('#type_service').val();
    blockUI();
    $.ajax({
        url: "{{ url('/load') }}",
        type: "post",
        data: { username: $('#username_form').val(), password: $('#pass_form').val(), form: $('#name_form').val() },
        headers:{
            'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 	'application/x-www-form-urlencoded',
            'Authorization':	"bearer " + localStorage.getItem("token")
        },
        success: function(data, textStatus, xhr)
        {
            Terms(data.terms, sType);
            Plans(data.plans, sType);
            if(sType == 2) { $('#ComercialData').attr("hidden", false);  }else{ $('#ComercialData').attr("hidden", true);  }
            $.unblockUI();
        },
        error: function( xhr, ajaxOptions, thrownError )
        {
            // localStorage.clear();
            // localStorage.setItem("error", JSON.parse(xhr.responseText).error);
            ErrorToken(JSON.parse(xhr.responseText).error);
            $.unblockUI();
        }
    });

});

function FinishForms()
{
    var terms   =   CheckTerms();

    if(terms)
    {
        $("#NextForm").find(':input').each(function()
        {
            var elemento= this;
            var final = elemento.id+'-error';
            $('head').append('<style>#'+final+'{ color: red; } </style>');
        });

        if($('#type_service').val() == 1)
        {
            $('#NextForm').validate({
                rules:{
                    type_service:   { required: true },
                    list_plan:		{ required: true },

                    name_com:		{ required: true, minlength: 2,  maxlength: 254},
                    rif_com:		{ required: true, minlength: 2,  maxlength: 254},
                    rep_com:		{ required: true, minlength: 2,  maxlength: 254},
                    email_com:		{ required: true, minlength: 10, maxlength: 50, email: true },

                    provider:		{ required: true, minlength: 2,  maxlength: 254},
                    ups:		    { required: true },

                },
                messages:{
                    type_service:	{
                        required: "Por favor seleccione un estado v&aacute;lida"},
                    list_plan:	{
                        required: "Por favor seleccione un estado v&aacute;lida"},
                    ups:	{
                        required: "Por favor seleccione un estado v&aacute;lida"},
                    provider:	{
                        required: "Por favor introduzca unas opciones v&aacute;lido", minlength: "Su opciones deben contener mas de 2 caracteres", maxlength: "Su opciones no debe exceder los 250 caracteres"},
                    name_com:	{
                        required: "Por favor introduzca un nombre comermcial v&aacute;lido", minlength: "Su nombre comermcial debe contener mas de 2 caracteres", maxlength: "Su nombre comermcial no debe exceder los 250 caracteres"},
                    rif_com:	{
                        required: "Por favor introduzca un rif comermcial v&aacute;lido", minlength: "Su rif comermcial debe contener mas de 2 caracteres", maxlength: "Su rif comermcial no debe exceder los 20 caracteres"},
                    rep_com:	{
                        required: "Por favor introduzca un representante comermcial v&aacute;lido", minlength: "Su representante comermcial debe contener mas de 2 caracteres", maxlength: "Su representante comermcial no debe exceder los 100 caracteres"},
                    email_com:	{
                        required: "Por favor introduzca un email v&aacute;lido", minlength: "Su email debe contener mas de 10 caracteres", maxlength: "Su email no debe exceder los 50 caracteres"},
                },submitHandler: function(form)
                {
                    blockUI();
                    $('#finish').html('Please Wait...');
                    $('#finish').attr("disabled", true);
                    $.ajax({
                        url: "{{ url('finishforms') }}",
                        type: "POST",
                        data: $('#NextForm').serialize(),
                        headers:{
                            'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
                            'Content-Type': 	'application/x-www-form-urlencoded',
                            'Authorization':	"bearer " + localStorage.getItem("token")
                        },
                        success: function(data, textStatus, xhr)
                        {
                            if(xhr.status == 200)
                            {
                                $('#NextForm')[0].reset();
                                $('#finish').html('Please Wait...');
                                $('#finish').attr("disabled", false);
                                $.unblockUI();
                                event.preventDefault();
                                Swal.fire({
                                    title: "¡Felicidades!",
                                    text: "Tu solicitud de instalación del servicio de Internet ha sido enviada con éxito.",
                                    footer: " Atento/a a nuestra llamada!  ",
                                    icon: 'success', showCancelButton: false, confirmButtonColor: '#3085d6', cancelButtonColor: '#d33', confirmButtonText: "Enviar Nueva Solicitud?"
                                    }).then((result) => {
                                    if (result.isConfirmed) {
                                        location.reload()
                                    }
                                });
                                return false;
                            }
                            $.unblockUI();
                        },
                        error: function( xhr, ajaxOptions, thrownError )
                        {
                            // localStorage.clear();
                            // localStorage.setItem("error", JSON.parse(xhr.responseText).error);
                            ErrorToken(JSON.parse(xhr.responseText).error);
                            $.unblockUI();
                        }
                    });
                }
            });
        }else{
            $('#NextForm').validate({
                rules:{
                    type_service:   { required: true },
                    list_plan:		{ required: true },

                    name_com:		{ required: true, minlength: 2,  maxlength: 254},
                    rif_com:		{ required: true, minlength: 2,  maxlength: 254},
                    rep_com:		{ required: true, minlength: 2,  maxlength: 254},
                    email_com:		{ required: true, minlength: 10, maxlength: 50, email: true },

                    provider:		{ required: true, minlength: 2,  maxlength: 254},
                    ups:		    { required: true },

                },
                messages:{
                    type_service:	{
                        required: "Por favor seleccione un estado v&aacute;lida"},
                    list_plan:	{
                        required: "Por favor seleccione un estado v&aacute;lida"},
                    ups:	{
                        required: "Por favor seleccione un estado v&aacute;lida"},
                    provider:	{
                        required: "Por favor introduzca unas opciones v&aacute;lido", minlength: "Su opciones deben contener mas de 2 caracteres", maxlength: "Su opciones no debe exceder los 250 caracteres"},
                    name_com:	{
                        required: "Por favor introduzca un nombre comermcial v&aacute;lido", minlength: "Su nombre comermcial debe contener mas de 2 caracteres", maxlength: "Su nombre comermcial no debe exceder los 250 caracteres"},
                    rif_com:	{
                        required: "Por favor introduzca un rif comermcial v&aacute;lido", minlength: "Su rif comermcial debe contener mas de 2 caracteres", maxlength: "Su rif comermcial no debe exceder los 20 caracteres"},
                    rep_com:	{
                        required: "Por favor introduzca un representante comermcial v&aacute;lido", minlength: "Su representante comermcial debe contener mas de 2 caracteres", maxlength: "Su representante comermcial no debe exceder los 100 caracteres"},
                    email_com:	{
                        required: "Por favor introduzca un email v&aacute;lido", minlength: "Su email debe contener mas de 10 caracteres", maxlength: "Su email no debe exceder los 50 caracteres"},
                },submitHandler: function(form)
                {
                    blockUI();
                    $('#finish').html('Please Wait...');
                    $('#finish').attr("disabled", true);
                    $.ajax({
                        url: "{{ url('finishforms') }}",
                        type: "POST",
                        data: $('#NextForm').serialize(),
                        headers:{
                            'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
                            'Content-Type': 	'application/x-www-form-urlencoded',
                            'Authorization':	"bearer " + localStorage.getItem("token")
                        },
                        success: function(data, textStatus, xhr)
                        {
                            if(xhr.status == 200)
                            {
                                $('#NextForm')[0].reset();
                                $('#finish').html('Please Wait...');
                                $('#finish').attr("disabled", false);
                                $.unblockUI();
                                event.preventDefault();
                                Swal.fire({
                                    title: "Felicidades!",
                                    text: "En breve momentos unos de nuestros representantes se estara comunciando contigo.",
                                    icon: 'success', showCancelButton: false, confirmButtonColor: '#3085d6', cancelButtonColor: '#d33', confirmButtonText: "Enviar Nueva Solicitud?"
                                    }).then((result) => {
                                    if (result.isConfirmed) {
                                        location.reload()
                                    }
                                });
                                return false;
                            }
                            $.unblockUI();
                        },
                        error: function( xhr, ajaxOptions, thrownError )
                        {
                            // localStorage.clear();
                            // localStorage.setItem("error", JSON.parse(xhr.responseText).error);
                            ErrorToken(JSON.parse(xhr.responseText).error);
                            $.unblockUI();
                        }
                    });
                }
            });
        }
    }else{
        Swal.fire(
            'T&eacute;rminos y condiciones!',
            'Lee detenidamente y acepta los t&eacute;rminos y condiciones del servicio para completar la solicitud',
            'error'
        );
    }

}

function restricted(e)
{
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla==8) return true;
    patron =/[A-Za-z\s]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

$("#age").datepicker({
    changeMonth: true,
    changeYear: true,
    defaultViewDate: {year: '-18'},
    yearRange: "-80:+18",
    dateFormat: 'yy-mm-dd'
});

function CheckTerms()
{
    var term    =   0;
    var tcheck  =   $('#terms_check').prop('checked');
    for (i=1; i <= 100; i++) { if($("#terms"+i).length > 0) {  if ($("#terms"+i).is(':checked') == false) { term = term+1; } } }
    if( (term == 0) && ($('#term_check').prop('checked') ) ) { return true; }
    return false;
}

function Terms(data, type)
{
    $('#termsCondsTable').append('<table><tbody>');
    var c = 1;
    $('#termsCondsTable').html('');
    $('#termsCondsCheck').html('');
    for (i=1; i <= 100; i++) {  if($("#terms"+i).length > 0)  {   if ($("#terms"+i).is(':checked') == true) {  $("#terms"+i).prop('checked', false); }  }  }
    $("#term_res"+i).prop('checked', false)
    $("#term_com"+i).prop('checked', false)

    $.each(data, function (key, val)
    {
        if( (type == 1) && (val['package'] == 'residencial'))
        {
            $('#termsCondsTable').append('<tr>');
            $('#termsCondsTable').append('<td><input class="custom-checkbox text-center" type="checkbox" name="terms'+c+'" id="terms'+c+'" value="accepted" /></td>');
            $('#termsCondsTable').append('<td><b>&nbsp;' + c +' - </b>' + val['name'] +'</td>');
            $('#termsCondsTable').append('</tr>');
            c = c+1;

        }
        if( (type == 2) && (val['package'] == 'comercial'))
        {
            $('#termsCondsTable').append('<tr>');
            $('#termsCondsTable').append('<td><input class="custom-checkbox text-center" type="checkbox" name="terms'+c+'" id="terms'+c+'" value="accepted" /></td>');
            $('#termsCondsTable').append('<td><b>&nbsp;' + c +' - </b>' + val['name'] +'</td>');
            $('#termsCondsTable').append('</tr>');
            c = c+1;

        }
    });

    if(type == 1)
    {
        $('#termsCondsCheck').append('<input class="custom-checkbox text-center" type="checkbox" name="term_res" id="term_res" value="1"/>');
        $('#termsCondsCheck').append('<label for="term_check"><b>Le&iacute; los t&eacute;rminos y condiciones del contrato, <b>estoy de acuerdo con instalar el servicio</b></label>');
    }
    if(type == 2)
    {
        $('#termsCondsCheck').append('<input class="custom-checkbox text-center" type="checkbox" name="term_com" id="term_com" value="1"/>');
        $('#termsCondsCheck').append('<label for="term_check"><b>Le&iacute; los t&eacute;rminos y condiciones del contrato, <b>estoy de acuerdo con instalar el servicio</b></label>');
    }
}

function Plans(data, package)
{
    let typeNew =  [];
    let type =  [];
    let lm = c =0;

    $.each(data, function(key, val) {
        if((package == 1) && (val.package == "residencial")) { if(!type.includes(val.type)) { type.push(val.type); } }
        if((package == 2) && (val.package == "comercial")) { if(!type.includes(val.type)) { type.push(val.type); } }
    })

    $('#list_plan').html('');
    $.each(data, function(key, val) {
        if((package == 1) && (val.package == "residencial"))
        {
            $.each(type, function(k,v){
                if(val.type == v)
                {
                    $('#list_plan').append($("<option></option>").attr("value", val.uuid).text(val.name));
                }
            })
        }
        if((package == 2) && (val.package == "comercial"))
        {
            $.each(type, function(k,v){
                if(val.type == v)
                {
                    $('#list_plan').append($("<option></option>").attr("value", val.uuid).text(val.name));
                }
            })
        }
    })
}

function Services(data)
{
    $.each(data, function(key, val){ $('#type_service').append($("<option></option>").attr("value", val.id).text(val.name)); });
}

function Options(data, info)
{
    $.each(data, function(key, val){ $('#'+info+'').append($("<option></option>").attr("value", val.uuid).text(val.name));  });
}

function CheckTerms()
{
    var term    =   0;
    for (i=1; i <= 100; i++) { if($("#terms"+i).length > 0) {  if ($("#terms"+i).is(':checked') == false) { term = term+1; } } }
    if( (term == 0) && ($('#term_res').prop('checked') ) ) { return true; }
    if( (term == 0) && ($('#term_com').prop('checked') ) ) { return true; }
    if( (term > 0) && ($('#term_data').prop('checked') ) ) { return true; }

    return false;
}

function blockUI()
{
    $.blockUI({ css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .5,
        color: '#fff'
    }});
}

function ErrorToken(message)
{
    switch(message)
    {
        case "Invalid Token":               fTitle = 'Advertencia.!'; fText = 'Se esta enviando un token inválido';          fForm = "Recargar Pagina"; break;
        case "Token has Expired":           fTitle = 'Advertencia.!'; fText = 'Su token a experidado';                       fForm = "Recargar Pagina"; break;
        case "Token Not Parsed":            fTitle = 'Advertencia.!'; fText = 'Se deben enviar un token válido.';            fForm = "Recargar Pagina"; break;
        case "Could not create token":      fTitle = 'Advertencia.!'; fText = 'El token no pudo ser creado.';                fForm = "Recargar Pagina"; break;
        case "Invalid Credentials":         fTitle = 'Advertencia.!'; fText = 'Las credenciales enviadas son inválidas.';    fForm = "Recargar Pagina"; break;
        case "Dont have authorization":     fTitle = 'Advertencia.!'; fText = 'Sin permiso para ejecutar esta acción.';      fForm = "Recargar Pagina"; break;
        case "Content Type NO Supported":   fTitle = 'Advertencia.!'; fText = 'Contenido enviado no válido.';                fForm = "Recargar Pagina"; break;
        default: fTitle = ""; fText = ""; fForm = "";
    }

    Swal.fire({ title: fTitle, text: fText, icon: 'warning', showCancelButton: false, confirmButtonColor: '#3085d6', cancelButtonColor: '#d33', confirmButtonText: fForm
        }).then((result) => {
        if (result.isConfirmed) {
            location.reload()
        }
    })
}

function SuccessMessage(message)
{
    switch(message)
    {
        case "Country": fTitle = '¡Los Datos almacenados correctamente!'; fText = 'Lamentamos informarte de que aún no poseemos servicio para donde solicitas la instalación, pronto nos contactaremos contigo.'; fForm = "Aceptar"; break;
        default: fTitle = ""; fText = ""; fForm = "";
    }

    Swal.fire({ title: fTitle, text: fText, icon: 'success', showCancelButton: false, confirmButtonColor: '#3085d6', cancelButtonColor: '#d33', confirmButtonText: fForm
        }).then((result) => {
        if (result.isConfirmed) {
            location.reload()
        }
    })
}

var timeoutMap;

$( "#address" ).keypress(function()
{
    if(timeoutMap)
    {
        clearTimeout(timeoutMap);
        timeout = null;
    }
    timeoutMap = setTimeout( () => {

        $.ajax({
            url: "{{ url('/forms/curl') }}",
            type: "POST",
            // data:   { add: $('#street').val() + ' ' + $('#maps').val() + ' ' + $('#city').val() },
            data:   { add: $('#address').val() },
            headers:{
                'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
                'Content-Type': 	'application/x-www-form-urlencoded',
                'Authorization':	"bearer " + localStorage.getItem("token")
            },
            success: function(data, textStatus, xhr)
            {
                if(xhr.status == 200)
                {
                    initMap2(data);
                    return false;
                }
                $.unblockUI();
            }
        });


    }, 750);

    // clearTimeout(timeout);
});

function initMap2(data)
{
    navigator.geolocation.getCurrentPosition(
        function (position) {
            coords = {
                lng: data.longitud,
                lat: data.latitud
            };
            document.getElementById("latitud").value = data.latitud;
            document.getElementById("longitud").value = data.longitud;
            setMapa2(coords);

        },function (error) {
            console.log(error);
        }
    );
}

function setMapa2(coords)
{
    var map = new google.maps.Map(document.getElementById('map'),
    {
        zoom: 18,
        mapTypeId: 'hybrid',
        center: new google.maps.LatLng(coords.lat,coords.lng),
    });

    marker = new google.maps.Marker({
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        position: new google.maps.LatLng(coords.lat,coords.lng),
    });

    marker.addListener('click', toggleBounce);

    marker.addListener('dragend', function (event) {
        document.getElementById("latitud").value = this.getPosition().lat();
        document.getElementById("longitud").value = this.getPosition().lng();
    });
}

function toggleBounce()
{
    if (marker.getAnimation() !== null)
    {
        marker.setAnimation(null);
    } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
    }

}

</script>
