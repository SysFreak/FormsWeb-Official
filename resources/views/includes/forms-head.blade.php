	<meta charset="utf-8">
	<meta name="viewport"       content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description"    content="BoomSolutions">
	<meta name="author"         content="Ing. Luis Campos">
	<link rel="icon"            href="{{ asset('assets/img/logo.png') }}">
	<title>Boom Solutions Venezuela</title>
	<link rel="canonical"       href="https://getbootstrap.com/docs/4.0/examples/checkout/">
	<!-- Bootstrap core CSS -->
	<link href="{{ asset('assets/css/bootstrap.min.css') }}"          rel="stylesheet">
	<link href="{{ asset('assets/css/parsley.css') }}"                rel="stylesheet">
	<!-- Custom styles for this template -->

	<link href="{{ asset('assets/css/form-validation.css') }}"        rel="stylesheet">
	<link href="{{ asset('assets/css/font-awesome.min.css') }}"       rel="stylesheet">
	<link href="{{ asset('img/cropped-favicon-180x180.png') }}"       rel="shortcut icon" type="image/x-icon">

	<meta name="csrf-token" content="{{ csrf_token() }}" />

	<!-- DATEPICKER START -->

	<link rel="stylesheet" href="{{ asset('assets/css/daterangepicker.css') }}" type="text/css"/>
	<link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.css') }}">

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmScbfZ1jRKJH262lqWAfX_HGBbgra1Ao&libraries=places&callback=initMap" async defer></script>
	<script src="{{ asset('assets/js/map.js') }}">					type="text/javascript"</script>


	<!-- DATEPICKER END -->

	<style>
		#map {
            margin: 0 auto;
            width: 100%;
            height: 250px;
		}

		#coords {
			width: 500px;
		}

	</style>
