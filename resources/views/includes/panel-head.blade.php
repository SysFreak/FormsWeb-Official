<meta charset="utf-8">
<meta name="viewport"       content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description"    content="Forms Web to Sales - BoomSolutions">
<meta name="author"         content="Ing. Luis Campos">
<link rel="icon"            href="{{ asset('assets/img/logo.png') }}">
<title>Boom Solutions - Venezuela</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<link rel="stylesheet" href="{{ asset('panel/plugins/fontawesome-free/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('panel/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('panel/dist/css/adminlte.min.css') }}">
<link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
<meta name="csrf-token" content="{{ csrf_token() }}" />