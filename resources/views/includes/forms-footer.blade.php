    <script src="{{ asset('assets/js/3.2.1.jquery.min.js') }}">		type="text/javascript"</script>
	<script src="{{ asset('assets/js/popper.min.js') }}">			type="text/javascript"</script>
	<script src="{{ asset('assets/js/bootstrap.min.js') }}">		type="text/javascript"</script>
	<script src="{{ asset('assets/js/holder.min.js') }}">			type="text/javascript"</script>
	<script src="{{ asset('assets/js/jquery.validate.min.js') }}">	type="text/javascript"</script>
	<script src="{{ asset('assets/js/parsley.js') }}">				type="text/javascript"</script>
	<script src="{{ asset('assets/js/blockUI.js') }}">				type="text/javascript"</script>
	<script src="{{ asset('assets/js/sweetalert2@11.js') }}">		type="text/javascript"</script>

	<!-- DATEPICKER START -->

	<script src="{{ asset('assets/js/moment.min.js') }}"> 			type="text/javascript"</script>
	<script src="{{ asset('assets/js/daterangepicker.min.js') }}"> 	type="text/javascript"</script>
	<script src="{{ asset('assets/js/jquery-ui.js') }}">			type="text/javascript"</script>
