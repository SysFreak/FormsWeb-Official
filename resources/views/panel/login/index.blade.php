@extends('layouts.panel-login')

@section('content')

<div class="card card-outline card-primary">
  <div class="card-header text-center">
    <a href="{{  route('/cpanel') }}" class="h1"><img class="w-50" src="{{ asset('assets/img/logocolor.png') }}" alt="Logo de Boom Solutions"></a>
  </div>
  <div class="card-body">
    <p class="login-box-msg">Inicio de Sesi&oacute;n</p>

    <form action="{{  route('/cpanel/dashboard') }}" method="post">
    @csrf
      @foreach($errors->all() as $error)
        <div> <div class="alert alert-danger" style="font-size: 10px;">{{$error}}</div></div>
      @endforeach
      <div class="input-group mb-3">
        <input type="text" class="form-control" id="username" name="username" placeholder="Username">
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-user"></span>
          </div>
        </div>
      </div>
      <div class="input-group mb-3">
        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-lock"></span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
        <button type="submit" class="btn btn-primary btn-block">Sign In</button>
        </div>
      </div>
    </form>
    <p class="mb-1">
      <a href="forgot-password.html">Olvid&eacute; mi password</a>
    </p>
  </div>
</div>

@endsection