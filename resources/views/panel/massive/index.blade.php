@extends('layouts.panel')

@section('content')

@include('panel.dashboard.navbar')

@include('panel.dashboard.aside')

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Panel de Control - Carga massiva</h1>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-xs-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Listado de Formularios</h3>
              </div>
              <div class="card-body">
                <table id="uploadMassiveTable" class="table table-bordered table-hover">
                @csrf
                  <thead style="text-align: center;">
                  <tr>
                    <th>id</th>
                    <th>Nombre</th>
                    <th>Tel&eacute;fono</th>
                    <th>Status</th>
                  </tr>
                  </thead>
                  <tbody style="text-align: center;">
                  </tbody>
                </table>
              </div>
              <div class="card-footer">
                  <div class="row">
                      <div class="col-md-4">
                        <a type="button" class="btn btn-block btn-outline-dark btn-xs" onclick="processModal();">Procesar</a>
                      </div>
                      <div class="col-md-4"></div>
                      <div class="col-md-4">
                        <a type="button" class="btn btn-block btn-outline-dark btn-xs" onclick="uploadModal();">Cargar Archivos</a>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>

  @include('panel.massive.modal')

  @include('panel.dashboard.footer')

  @endsection

  @section('js')
    @include('panel.massive.js')
  @endsection