<script>

    load();

    function load()
    {
        $.ajax({
        url: "{{ url('/cpanel/massive/load') }}",
        type: "GET",
        success: function(datos, textStatus, xhr)
        {          
            if(xhr.status == 200)
            {   
                if(datos.success == true)
                {
                    $('#uploadMassiveTable').append('<table>');
                    $.each(datos.data, function(key, val)
                    { 
                        $('#uploadMassiveTable').append('<tr><td>'+val.id+'</td><td>'+val.name+'</td><td>'+val.phone+'</td><td><span class="badge bg-success">'+val.status+'</span></td></tr>');
                    });

                    $('#uploadMassiveTable').append('</table>');
                    $('#uploadMassiveTable').DataTable({ "paging": true, "responsive": true, "pageLength": 3,});

                }else{
                    $('#uploadMassiveTable').html('');
                    $('#uploadMassiveTable').DataTable();
                }
                
            }
        },
        error: function( xhr, ajaxOptions, thrownError )
        {
            localStorage.clear();
            localStorage.setItem("error", JSON.parse(xhr.responseText).error);
            ErrorToken(JSON.parse(xhr.responseText).error);
        }
        });
    }


    function uploadModal()
    {
        $('#modal-upload').modal('show');
    }

    function processModal()
    {
        $.ajax({
        url: "{{ url('/cpanel/massive/process') }}",
        type: "POST",
        headers:{
            'X-CSRF-TOKEN': "{{csrf_token()}}"
        },
        success: function(datos, textStatus, xhr)
        {          
            if(xhr.status == 200)
            {   
                if(datos.success == true)
                {
                    $('#uploadMassiveTable').append('<table>');
                    $.each(datos.data, function(key, val)
                    { 
                        $('#uploadMassiveTable').append('<tr><td>'+val.id+'</td><td>'+val.name+'</td><td>'+val.phone+'</td><td><span class="badge bg-success">'+val.status+'</span></td></tr>');
                    });

                    $('#uploadMassiveTable').append('</table>');
                    $('#uploadMassiveTable').DataTable({ "paging": true, "responsive": true, "pageLength": 3,});

                }else{
                    $('#uploadMassiveTable').html('');
                    $('#uploadMassiveTable').DataTable();
                }
                
            }
        },
        error: function( xhr, ajaxOptions, thrownError )
        {
            localStorage.clear();
            localStorage.setItem("error", JSON.parse(xhr.responseText).error);
            ErrorToken(JSON.parse(xhr.responseText).error);
        }
        });
    }

    Dropzone.options.uploadFileDropzone = {
        headers:{
            'X-CSRF-TOKEN': "{{csrf_token()}}"
        },
        dictDefaultMessage: "Arrastre un archivo al recuadro para subirlo",
        acceptedFiles: ".csv, text/csv, application/vnd.ms-excel, application/csv, text/x-csv, application/x-csv, text/comma-separated-values, text/x-comma-separated-values",
        success: function(file, done) {
            if (done.success == true) 
            {
                this.removeAllFiles();
                $('#modal-upload').modal('hide');
                $('#uploadMassiveTable').append('<table>');
                $.each(datos.data, function(key, val)
                { 
                    $('#uploadMassiveTable').append('<tr><td>'+val.id+'</td><td>'+val.name+'</td><td>'+val.phone+'</td><td><span class="badge bg-success">'+val.status+'</span></td></tr>');
                });

                $('#uploadMassiveTable').append('</table>');
                $('#uploadMassiveTable').DataTable({ "paging": true, "responsive": true, "pageLength": 3,});

                processModal();


            }   else { 
                done();
            }; 
        }
    };

</script>