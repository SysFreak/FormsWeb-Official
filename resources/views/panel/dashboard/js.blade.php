<script>
    $(document).ready(function() 
    {
        localStorage.clear();
        localStorage.setItem("token", $('#token').val());
    });

    setTimeout(
    function() 
    {
        load(0)
    }, 
1000);

function load()
{
    $.ajax({
        url: "{{ url('/cpanel/forms') }}",
        type: "get",
        data: { token: localStorage.getItem('token') },
        headers:{
            'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 	'application/x-www-form-urlencoded'
        },
        success: function(data, textStatus, xhr)
        {
            if(xhr.status == 200)
            {   
                localStorage.clear();
                localStorage.setItem("token", data.token);

                // Modals Forms Name
                $('#FormsNameTable').append('<table>');
                $.each(data.formsTab, function(key, val){
                $('#FormsNameTable').append('<tr><td>'+val.name+'</td><td>'+val.type+'</td><td><span class="badge bg-success">'+val.status+'</span></td><td><button class="btn btn-block btn-outline-dark btn-xs" id="NTEdit'+(val.id)+'" value="'+val.uuid+'" onclick="NameFormsEdit('+val.id+');">Edit</button></td></tr>') });
                $('#FormsNameTable').append('</table>');
                $('#FormsNameTable').DataTable({ "paging": true, "responsive": true, "pageLength": 3,});

                $.each(data.ftype, function(key, val)
                { 
                    $('#type_forms').append($("<option></option>").attr("value", val.uuid).text(val.name));
                    $('#type_forms_edits').append($("<option></option>").attr("value", val.uuid).text(val.name)); 
                    $('#type_users_edits').append($("<option></option>").attr("value", val.uuid).text(val.name)); 
                    $('#type_users').append($("<option></option>").attr("value", val.uuid).text(val.name)); 
                });
                $.each(data.status, function(key, val){ 
                    $('#status_forms_edits').append($("<option></option>").attr("value", val.uuid).text(val.name)); 
                    $('#status_types_edits').append($("<option></option>").attr("value", val.uuid).text(val.name)); 
                    $('#status_users_edits').append($("<option></option>").attr("value", val.uuid).text(val.name)); 
                    $('#e_status_emails').append($("<option></option>").attr("value", val.uuid).text(val.name)); 
                });
                
                $.each(data.formsTab, function(key, val){ 
                    $('#forms_users').append($("<option></option>").attr("value", val.uuid).text(val.name)); 
                    $('#forms_users_edits').append($("<option></option>").attr("value", val.uuid).text(val.name)); 
                    $('#form_emails').append($("<option></option>").attr("value", val.uuid).text(val.name)); 
                    $('#e_form_emails').append($("<option></option>").attr("value", val.uuid).text(val.name)); 
                });
                
                // END POINT

                // Modals Forms Type
                $('#FormsTypeName').append('<table>');
                $.each(data.ftype, function(key, val){
                $('#FormsTypeName').append('<tr><td>'+val.name+'</td><td><span class="badge bg-success">'+val.status+'</span></td><td><button class="btn btn-block btn-outline-dark btn-xs" id="NTTEdit'+key+'" value="'+val.uuid+'" onclick="TypeFormsEdit('+key+');">Edit</button></td></tr>') });
                $('#FormsTypeName').append('</table>');
                $('#FormsTypeName').DataTable({ "paging": true, "responsive": true, "pageLength": 3,});

                // Modals Users Forms Name
                $('#FormsUsersName').append('<table>');
                $.each(data.fusers, function(key, val){
                $('#FormsUsersName').append('<tr><td>'+val.username+'</td><td>'+val.email+'</td><td>'+val.forms+'</td><td>'+val.type+'</td><td><span class="badge bg-success">'+val.status+'</span></td><td><button class="btn btn-block btn-outline-dark btn-xs" id="UFEdit'+key+'" value="'+val.uuid+'" onclick="UsersFormsEdit('+key+');">Edit</button></td></tr>') });
                $('#FormsUsersName').append('</table>');
                $('#FormsUsersName').DataTable({ "paging": true, "responsive": true, "pageLength": 3,});

                // Modals Users Forms Name
                $('#FormsEmailsType').append('<table>');
                let type = '';
                $.each(data.femail, function(key, val){
                if(val.email_type == 1){ type = "Principal" ; }else { type = "Copia"; }
                $('#FormsEmailsType').append('<tr><td>'+key+'</td><td>'+val.email+'</td><td>'+type+'</td><td>'+val.forms+'</td><td><span class="badge bg-success">'+val.status+'</span></td><td><button class="btn btn-block btn-outline-dark btn-xs" id="LEEdit'+key+'" value="'+val.uuid+'" onclick="ListEmailEdit('+key+');">Edit</button></td></tr>') });
                $('#FormsEmailsType').append('</table>');
                $('#FormsEmailsType').DataTable({ "paging": true, "responsive": true, "pageLength": 3,});
                
                
            }
        },
        error: function( xhr, ajaxOptions, thrownError )
        {
            localStorage.clear();
            localStorage.setItem("error", JSON.parse(xhr.responseText).error);
            ErrorToken(JSON.parse(xhr.responseText).error);
        }
    });
}
    

function ErrorToken(message)
{
   
    switch(message)
    {
        case "Invalid Token":               fTitle = 'Advertencia.!'; fText = 'Se esta enviando un token inválido';          fForm = "Recargar Pagina"; break;
        case "Token has Expired":           fTitle = 'Advertencia.!'; fText = 'Su token a experidado';                       fForm = "Recargar Pagina"; break;
        case "Token Not Parsed":            fTitle = 'Advertencia.!'; fText = 'Se deben enviar un token válido.';            fForm = "Recargar Pagina"; break;
        case "Could not create token":      fTitle = 'Advertencia.!'; fText = 'El token no pudo ser creado.';                fForm = "Recargar Pagina"; break;
        case "Invalid Credentials":         fTitle = 'Advertencia.!'; fText = 'Las credenciales enviadas son inválidas.';    fForm = "Recargar Pagina"; break;
        case "Dont have authorization":     fTitle = 'Advertencia.!'; fText = 'Sin permiso para ejecutar esta acción.';      fForm = "Recargar Pagina"; break;  
        case "Content Type NO Supported":   fTitle = 'Advertencia.!'; fText = 'Contenido enviado no válido.';                fForm = "Recargar Pagina"; break;  
        default: fTitle = ""; fText = ""; fForm = "";          
    }
    
        Swal.fire({ title: fTitle, text: fText, icon: 'warning', showCancelButton: false, confirmButtonColor: '#3085d6', cancelButtonColor: '#d33', confirmButtonText: fForm
        }).then((result) => {
        if (result.isConfirmed) {   window.location.replace("/cpanel");           }
        })
}


function NewFormsClients()
{    
    $.ajax({
        url: "{{ url('/cpanel/formsRegister') }}",
        type: "POST",
        data: { token: localStorage.getItem('token'), data: $('#NewFormsFormRegister').serialize() },
        headers:{
            'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 	'application/x-www-form-urlencoded'
        },
        success: function(data, textStatus, xhr)
        {          
            localStorage.clear();
            localStorage.setItem("token", data.token);
            if(xhr.status == 200)
            {   

                if(data.status == true)
                {
                    localStorage.clear();
                    localStorage.setItem("token", data.token);
                    $('#NewFormsFormRegister')[0].reset();
                    $('#modal-forms').modal('hide');
                }else{
                    $('#RegisterFormAlert').html('<div class="alert alert-warning alert-dismissible">'+data.message+'</div>');
                }
                
            }
        },
        error: function( xhr, ajaxOptions, thrownError )
        {
            localStorage.clear();
            localStorage.setItem("error", JSON.parse(xhr.responseText).error);
            ErrorToken(JSON.parse(xhr.responseText).error);
        }
    });
}

function NameFormsEdit(id)
{
    $.ajax({
        url: "{{ url('/cpanel/formsEdit') }}",
        type: "POST",
        data: { token: localStorage.getItem('token'), id: $('#NTEdit'+id).val() },
        headers:{
            'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 	'application/x-www-form-urlencoded'
        },
        success: function(data, textStatus, xhr)
        {
            if(xhr.status == 200)
            {   
                localStorage.clear();
                localStorage.setItem("token", data.token);
                $('#FNameEdit')[0].reset();
                $('#id_forms_edits').val(data.forms.id);
                $('#name_forms_id').val(data.forms.uuid);
                $('#name_forms_edits').val(data.forms.name);
                $('#modal-forms-edit').modal('show');
            }
        },
        error: function( xhr, ajaxOptions, thrownError )
        {
            localStorage.clear();
            localStorage.setItem("error", JSON.parse(xhr.responseText).error);
            ErrorToken(JSON.parse(xhr.responseText).error);
        }
    });
}

function NewTypesForms()
{
    $.ajax({
        url: "{{ url('/cpanel/formsTypesRegister') }}",
        type: "POST",
        data: { token: localStorage.getItem('token'), data: $('#NewModalTypes').serialize() },
        headers:{
            'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 	'application/x-www-form-urlencoded'
        },
        success: function(data, textStatus, xhr)
        {          
            localStorage.clear();
            localStorage.setItem("token", data.token);
            if(xhr.status == 200)
            {   

                if(data.status == true)
                {
                    localStorage.clear();
                    localStorage.setItem("token", data.token);
                    $('#NewModalTypes')[0].reset();
                    $('#modal-types').modal('hide');
                }else{
                    $('#ModalTypesAlert').html('<div class="alert alert-warning alert-dismissible">'+data.message+'</div>');
                }
                
            }
        },
        error: function( xhr, ajaxOptions, thrownError )
        {
            localStorage.clear();
            localStorage.setItem("error", JSON.parse(xhr.responseText).error);
            ErrorToken(JSON.parse(xhr.responseText).error);
        }
    }); 
}

function EditFormsClients()
{
    $.ajax({
        url: "{{ url('/cpanel/formsUpdate') }}",
        type: "PUT",
        data: { token: localStorage.getItem('token'), data: $('#FNameEdit').serialize() },
        headers:{
            'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 	'application/x-www-form-urlencoded'
        },
        success: function(data, textStatus, xhr)
        {          
            localStorage.clear();
            localStorage.setItem("token", data.token);
            if(xhr.status == 200)
            {   

                if(data.status == true)
                {
                    localStorage.clear();
                    localStorage.setItem("token", data.token);
                    $('#FNameEdit')[0].reset();
                    $('#modal-forms-edit').modal('hide');
                }else{
                    $('#RegisterFormEditAlert').html('<div class="alert alert-warning alert-dismissible">'+data.message+'</div>');
                }
                
            }
        },
        error: function( xhr, ajaxOptions, thrownError )
        {
            localStorage.clear();
            localStorage.setItem("error", JSON.parse(xhr.responseText).error);
            ErrorToken(JSON.parse(xhr.responseText).error);
        }
    });
}

function TypeFormsEdit(id)
{
    $.ajax({
        url: "{{ url('/cpanel/formsTypesEdit') }}",
        type: "post",
        data: { token: localStorage.getItem('token'), id: $('#NTTEdit'+id).val() },
        headers:{
            'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 	'application/x-www-form-urlencoded'
        },
        success: function(data, textStatus, xhr)
        {          
            localStorage.clear();
            localStorage.setItem("token", data.token);
            if(xhr.status == 200)
            {   

                if(data.status == true)
                {
                    localStorage.clear();
                    localStorage.setItem("token", data.token);
                    $('#FTypeEdit')[0].reset();
                    $('#id_types_edits').val(data.forms[0].uuid);
                    $('#name_types_edits').val(data.forms[0].name);
                    $('#modal-types-edit').modal('show');
                }else{
                    $('#TypeFormEditAlert').html('<div class="alert alert-warning alert-dismissible">'+data.message+'</div>');
                }
                
            }
        },
        error: function( xhr, ajaxOptions, thrownError )
        {
            localStorage.clear();
            localStorage.setItem("error", JSON.parse(xhr.responseText).error);
            ErrorToken(JSON.parse(xhr.responseText).error);
        }
    });
}

function EditFormsTypes()
{
    $.ajax({
        url: "{{ url('/cpanel/formsTypesUpdate') }}",
        type: "PUT",
        data: { token: localStorage.getItem('token'), data: $('#FTypeEdit').serialize() },
        headers:{
            'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 	'application/x-www-form-urlencoded'
        },
        success: function(data, textStatus, xhr)
        {          
            localStorage.clear();
            localStorage.setItem("token", data.token);
            if(xhr.status == 200)
            {   

                if(data.status == true)
                {
                    localStorage.clear();
                    localStorage.setItem("token", data.token);
                    $('#FTypeEdit')[0].reset();
                    $('#modal-types-edit').modal('hide');
                }else{
                    $('#TypeFormEditAlert').html('<div class="alert alert-warning alert-dismissible">'+data.message+'</div>');
                }
                
            }
        },
        error: function( xhr, ajaxOptions, thrownError )
        {
            localStorage.clear();
            localStorage.setItem("error", JSON.parse(xhr.responseText).error);
            ErrorToken(JSON.parse(xhr.responseText).error);
        }
    });
}


function UsersFormsEdit(id)
{

    $.ajax({
        url: "{{ url('/cpanel/formsUsersEdit') }}",
        type: "post",
        data: { token: localStorage.getItem('token'), id: $('#UFEdit'+id).val() },
        headers:{
            'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 	'application/x-www-form-urlencoded'
        },
        success: function(data, textStatus, xhr)
        {          
            localStorage.clear();
            localStorage.setItem("token", data.token);
            if(xhr.status == 200)
            {   

                if(data.status == true)
                {
                    localStorage.clear();
                    localStorage.setItem("token", data.token);
                    $('#FUsersEdit')[0].reset();
                    $('#id_users_edits').val(data.forms.uuid);
                    $('#name_users_edits').val(data.forms.username);
                    $('#email_users_edits').val(data.forms.email);
                    $('#modal-users-edit').modal('show');
                }else{
                    $('#UsersFormEditAlert').html('<div class="alert alert-warning alert-dismissible">'+data.message+'</div>');
                }
                
            }
        },
        error: function( xhr, ajaxOptions, thrownError )
        {
            localStorage.clear();
            localStorage.setItem("error", JSON.parse(xhr.responseText).error);
            ErrorToken(JSON.parse(xhr.responseText).error);
        }
    });
}

function EditFormsUsers()
{
    $.ajax({
        url: "{{ url('/cpanel/formsUsersUpdate') }}",
        type: "PUT",
        data: { token: localStorage.getItem('token'), data: $('#FUsersEdit').serialize() },
        headers:{
            'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 	'application/x-www-form-urlencoded'
        },
        success: function(data, textStatus, xhr)
        {          
            localStorage.clear();
            localStorage.setItem("token", data.token);
            if(xhr.status == 200)
            {   

                if(data.status == true)
                {
                    localStorage.clear();
                    localStorage.setItem("token", data.token);
                    $('#FUsersEdit')[0].reset();
                    $('#modal-users-edit').modal('hide');
                }else{
                    $('#UsersFormEditAlert').html('<div class="alert alert-warning alert-dismissible">'+data.message+'</div>');
                }
                
            }
        },
        error: function( xhr, ajaxOptions, thrownError )
        {
            localStorage.clear();
            localStorage.setItem("error", JSON.parse(xhr.responseText).error);
            ErrorToken(JSON.parse(xhr.responseText).error);
        }
    });
}

function NewFormsUsers()
{
    $.ajax({
        url: "{{ url('/cpanel/formsUsersRegister') }}",
        type: "POST",
        data: { token: localStorage.getItem('token'), data: $('#FUsers').serialize() },
        headers:{
            'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 	'application/x-www-form-urlencoded'
        },
        success: function(data, textStatus, xhr)
        {          
            localStorage.clear();
            localStorage.setItem("token", data.token);
            if(xhr.status == 200)
            {   

                if(data.status == true)
                {
                    localStorage.clear();
                    localStorage.setItem("token", data.token);
                    $('#FUsers')[0].reset();
                    $('#modal-users').modal('hide');
                }else{
                    $('#UsersFormAlert').html('<div class="alert alert-warning alert-dismissible">'+data.message+'</div>');
                }
                
            }
        },
        error: function( xhr, ajaxOptions, thrownError )
        {
            localStorage.clear();
            localStorage.setItem("error", JSON.parse(xhr.responseText).error);
            ErrorToken(JSON.parse(xhr.responseText).error);
        }
    }); 
}

function ListEmailEdit(id)
{

    $.ajax({
        url: "{{ url('/cpanel/formsEmailsEdit') }}",
        type: "post",
        data: { token: localStorage.getItem('token'), id: $('#LEEdit'+id).val() },
        headers:{
            'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 	'application/x-www-form-urlencoded'
        },
        success: function(data, textStatus, xhr)
        {          
            localStorage.clear();
            localStorage.setItem("token", data.token);
            if(xhr.status == 200)
            {   

                if(data.status == true)
                {
                    localStorage.clear();
                    localStorage.setItem("token", data.token);
                    $('#FEmailsEdit')[0].reset();
                    $('#id_e_name_emails').val(data.forms.uuid);
                    $('#e_name_emails').val(data.forms.email);
                    $('#modal-emails-edit').modal('show');
                }else{
                    $('#EmailsListFormAlert').html('<div class="alert alert-warning alert-dismissible">'+data.message+'</div>');
                }
                
            }
        },
        error: function( xhr, ajaxOptions, thrownError )
        {
            localStorage.clear();
            localStorage.setItem("error", JSON.parse(xhr.responseText).error);
            ErrorToken(JSON.parse(xhr.responseText).error);
        }
    });
}

function EditListEmails()
{
    $.ajax({
        url: "{{ url('/cpanel/formsEmailsUpdate') }}",
        type: "PUT",
        data: { token: localStorage.getItem('token'), data: $('#FEmailsEdit').serialize() },
        headers:{
            'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 	'application/x-www-form-urlencoded'
        },
        success: function(data, textStatus, xhr)
        {          
            localStorage.clear();
            localStorage.setItem("token", data.token);
            if(xhr.status == 200)
            {   

                if(data.status == true)
                {
                    localStorage.clear();
                    localStorage.setItem("token", data.token);
                    $('#FEmailsEdit')[0].reset();
                    $('#modal-emails-edit').modal('hide');
                }else{
                    $('#EmailsListFormAlert').html('<div class="alert alert-warning alert-dismissible">'+data.message+'</div>');
                }
                
            }
        },
        error: function( xhr, ajaxOptions, thrownError )
        {
            localStorage.clear();
            localStorage.setItem("error", JSON.parse(xhr.responseText).error);
            ErrorToken(JSON.parse(xhr.responseText).error);
        }
    });
}

function NewFormsEmails()
{
    $.ajax({
        url: "{{ url('/cpanel/formsEmailsRegister') }}",
        type: "POST",
        data: { token: localStorage.getItem('token'), data: $('#FEmails').serialize() },
        headers:{
            'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 	'application/x-www-form-urlencoded'
        },
        success: function(data, textStatus, xhr)
        {          
            localStorage.clear();
            localStorage.setItem("token", data.token);
            if(xhr.status == 200)
            {   

                if(data.status == true)
                {
                    localStorage.clear();
                    localStorage.setItem("token", data.token);
                    $('#FEmails')[0].reset();
                    $('#modal-emails').modal('hide');
                }else{
                    $('#EmailsFormAlert').html('<div class="alert alert-warning alert-dismissible">'+data.message+'</div>');
                }
                
            }
        },
        error: function( xhr, ajaxOptions, thrownError )
        {
            localStorage.clear();
            localStorage.setItem("error", JSON.parse(xhr.responseText).error);
            ErrorToken(JSON.parse(xhr.responseText).error);
        }
    }); 
}

function SubmitType()
{
    if($('#name_types').val() == '')
    {
        $('#ModalTypesAlert').html('<div class="alert alert-warning alert-dismissible">Debe introducir todos los datos validos.</div>');            
    }else{
        $('#ModalTypesAlert').html('');
        $.ajax({
            url: "{{ url('/cpanel/types') }}",
            type: "get",
            data: { token: localStorage.getItem('token'), name: $('#name_types').val() },
            headers:{
                'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
                'Content-Type': 	'application/x-www-form-urlencoded'
            },
            success: function(data, textStatus, xhr)
            {
                if(xhr.status == 200)
                {   
                    localStorage.clear();
                    localStorage.setItem("token", data.token);

                    $.each(data.ftype, function(key, val){ $('#type_forms').append($("<option></option>").attr("value", val.uuid).text(val.name)); });
                    $.each(data.ftype, function(key, val){ $('#type_forms_edits').append($("<option></option>").attr("value", val.uuid).text(val.name)); });
                }
            },
            error: function( xhr, ajaxOptions, thrownError )
            {
                localStorage.clear();
                localStorage.setItem("error", JSON.parse(xhr.responseText).error);
                ErrorToken(JSON.parse(xhr.responseText).error);
            }
        });
    }
}

// function TypeFormsEdit(asdfasd)
// {
//     console.log(asdfasd);
// }

function SuccessMessage(message)
{
    switch(message)
    {
        case "Country": fTitle = '¡Los Datos almacenados correctamente!'; fText = 'Lamentamos informarte de que aún no poseemos servicio para donde solicitas la instalación, pronto nos contactaremos contigo.'; fForm = "Aceptar"; break;
        default: fTitle = ""; fText = ""; fForm = "";          
    }
    
    Swal.fire({ title: fTitle, text: fText, icon: 'success', showCancelButton: false, confirmButtonColor: '#3085d6', cancelButtonColor: '#d33', confirmButtonText: fForm
        }).then((result) => {
        if (result.isConfirmed) {
            location.reload()
        }
    })
}
</script>