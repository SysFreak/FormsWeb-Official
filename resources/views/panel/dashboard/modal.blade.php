<div class="modal fade" id="modal-forms">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Creaci&oacute;n de Formularios</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form id="NewFormsFormRegister">
                <div class="row"><div id="RegisterFormAlert" class="col-sm-12"></div></div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" class="form-control" name="name_forms" id="name_forms" placeholder="Nombre del Formulario">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Tipo</label>
                            <select class="form-control" name="type_forms" id="type_forms">
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button onclick="NewFormsClients()" class="btn btn-primary">Almacenar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-forms-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edici&oacute;n de Formulario</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="FNameEdit">
                    <div class="row"><div id="RegisterFormEditAlert" class="col-sm-12"></div></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" class="form-control" name="id_forms_edits" id="id_forms_edits" hidden>
                                    <input type="text" class="form-control" name="name_forms_edits" id="name_forms_edits" placeholder="Nombre del Formulario">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Tipo</label>
                                <select class="form-control" name="type_forms_edits" id="type_forms_edits">
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="status_forms_edits" id="status_forms_edits">
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button onclick="EditFormsClients()" class="btn btn-primary">Actualizar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-types">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tipos de Formularios</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @csrf
            <form id="NewModalTypes" name="NewModalTypes" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12" id="ModalTypesAlert">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" class="form-control" name="name_types" id="name_types" placeholder="Nombre">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Usuarios Asociados</label>
                                <select class="form-control" name="op_types" id="op_types">
                                    <option value="0"> NO</option>
                                    <option value="1"> SI</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" onclick="NewTypesForms()">Registrar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-types-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tipos de Formularios</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @csrf
            <form id="FTypeEdit">
                <div class="modal-body">
                    <div class="row"><div id="TypeFormEditAlert" class="col-sm-12"></div></div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" class="form-control" name="id_types_edits" id="id_types_edits" hidden>
                                <input type="text" class="form-control" name="name_types_edits" id="name_types_edits" placeholder="Nombre">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Usuarios Asociados</label>
                                <select class="form-control" name="op_types_edits" id="op_types_edits">
                                    <option value="0"> NO</option>
                                    <option value="1"> SI</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="status_types_edits" id="status_types_edits">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" onclick="EditFormsTypes()">Actualizar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-users">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Usuarios de Formularios</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @csrf
            <form id="FUsers">
                <div class="modal-body">
                    <div class="row"><div id="UsersFormAlert" class="col-sm-12"></div></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" name="name_users" id="name_users" placeholder="Nombre">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email_users" id="email_users" placeholder="Email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Formulario</label>
                                <select class="form-control" name="forms_users" id="forms_users">
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Tipo</label>
                                <select class="form-control" name="type_users" id="type_users">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" onclick="NewFormsUsers()">Actualizar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-users-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Usuarios de Formularios</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @csrf
            <form id="FUsersEdit">
                <div class="modal-body">
                    <div class="row"><div id="UsersFormEditAlert" class="col-sm-12"></div></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" name="id_users_edits" id="id_users_edits" hidden>
                                <input type="text" class="form-control" name="name_users_edits" id="name_users_edits" placeholder="Nombre">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email_users_edits" id="email_users_edits" placeholder="Email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Formulario</label>
                                <select class="form-control" name="forms_users_edits" id="forms_users_edits">
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Tipo</label>
                                <select class="form-control" name="type_users_edits" id="type_users_edits">
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="status_users_edits" id="status_users_edits">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" onclick="EditFormsUsers()">Actualizar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-emails">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Listado de Emails de Formularios</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @csrf
            <form id="FEmails">
                <div class="modal-body">
                    <div class="row"><div id="EmailsFormAlert" class="col-sm-12"></div></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" name="name_emails" id="name_emails" placeholder="Emails">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Formulario</label>
                                <select class="form-control" name="form_emails" id="form_emails">
                                </select>
                            </div>
                        </div> 
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Tipo</label>
                                <select class="form-control" name="type_emails" id="type_emails">
                                    <option value="1">Principal</option>
                                    <option value="2">Copia</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" onclick="NewFormsEmails()">Registrar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-emails-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Listado de Emails de Formularios</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @csrf
            <form id="FEmailsEdit">
                <div class="modal-body">
                    <div class="row"><div id="EmailsListFormAlert" class="col-sm-12"></div></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" name="id_e_name_emails" id="id_e_name_emails" placeholder="email" hidden>
                                <input type="text" class="form-control" name="e_name_emails" id="e_name_emails" placeholder="Emails">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Formulario</label>
                                <select class="form-control" name="e_form_emails" id="e_form_emails">
                                </select>
                            </div>
                        </div> 
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Tipo</label>
                                <select class="form-control" name="e_type_emails" id="e_type_emails">
                                    <option value="1">Principal</option>
                                    <option value="2">Copia</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="e_status_emails" id="e_status_emails">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" onclick="EditListEmails()">Actualizar</button>
                </div>
            </form>
        </div>
    </div>
</div>