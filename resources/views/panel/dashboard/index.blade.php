@extends('layouts.panel')

@section('content')

@include('panel.dashboard.navbar')

@include('panel.dashboard.aside')

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Panel de Control - Formularios</h1>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          

          <div class="col-6">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Listado de Formularios</h3>
                
                <div class="card-tools">
                  <button type="button" class="btn btn-block btn-outline-dark btn-xs" data-toggle="modal" data-target="#modal-forms">Nuevo</button>
                </div>
              </div>
              <div class="card-body">
                
                <table id="FormsNameTable" class="table table-bordered table-hover">
                @csrf
                  <thead style="text-align: center;">
                  <tr>
                    <th>Nombre</th>
                    <th>Tipo</th>
                    <th>Status</th>
                    <th>Edici&oacute;n</th>
                  </tr>
                  </thead>
                  <tbody style="text-align: center;">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-6">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Tipo de Formularios</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-block btn-outline-dark btn-xs" data-toggle="modal" data-target="#modal-types">Nuevo</button>
                </div>
              </div>
              <div class="card-body">
                <table id="FormsTypeName" class="table table-bordered table-hover">
                @csrf
                  <thead style="text-align: center;">
                  <tr>
                    <th>Nombre</th>
                    <th>Status</th>
                    <th>Edici&oacute;n</th>
                  </tr>
                  </thead>
                  <tbody style="text-align: center;">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Listado de Usuarios por formulario</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-block btn-outline-dark btn-xs" data-toggle="modal" data-target="#modal-users">Nuevo</button>
                </div>
              </div>
              <div class="card-body">
              <table id="FormsUsersName" class="table table-bordered table-hover">
                @csrf
                  <thead style="text-align: center;">
                  <tr>
                    <th>Usuario</th>
                    <th>Email</th>
                    <th>Formulario</th>
                    <th>Tipo</th>
                    <th>Status</th>
                    <th>Edici&oacute;n</th>
                  </tr>
                  </thead>
                  <tbody style="text-align: center;">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Listado de Emails asociados a formularios</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-block btn-outline-dark btn-xs" data-toggle="modal" data-target="#modal-emails">Nuevo</button>
                </div>
              </div>
              <div class="card-body">
              <table id="FormsEmailsType" class="table table-bordered table-hover">
                  <thead style="text-align: center;">
                  <tr>
                    <th>#</th>
                    <th>Email</th>
                    <th>Tipo</th>
                    <th>Formulario</th>
                    <th>Status</th>
                    <th>Edici&oacute;n</th>
                  </tr>
                  </thead>
                  <tbody style="text-align: center;">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>

  @include('panel.dashboard.modal')

  @include('panel.dashboard.footer')

  @endsection

  @section('js')
    @include('panel.dashboard.js')
  @endsection