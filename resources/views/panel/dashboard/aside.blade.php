<aside class="main-sidebar sidebar-dark-primary elevation-4">
    @csrf
    <a href="#" class="brand-link elevation-4">
        <img src="{{ asset('assets/img/logo.png') }}" alt="Boom Solutions" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span span class="brand-text font-weight-light">Boom Solutions</span>
    </a>

    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                @csrf
                <a href="#" class="d-block">{{ $username }}</a>
                <input type="text" class="form-control" id="token" name="token" value="{{ $token }}" hidden>
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p> Dashboard </p>
                    </a>
                </li>
                <li class="nav-item">
                    @csrf
                    <a href="{{ route('/cpanel/massive') }}" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p> Asignaci&oacute;n Masiva </p>
                    </a>
                    <input type="text" class="form-control" id="token" name="token" value="{{ $token }}" hidden>
                </li>
            </ul>
        </nav>
    </div>
</aside>