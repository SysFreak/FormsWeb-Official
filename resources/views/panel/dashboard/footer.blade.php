<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
        <b>Design By: </b> Ing. Luis Campos
    </div>
    <strong>Copyright &copy; <?php echo date("Y"); ?> <a href="https://boomsolutionsve.com">Boom Solutions</a>.</strong> All rights reserved.
  </footer>