@extends('layouts.forms')

@section('content')

<div class="card" style="color: #001c62;">

    <div class="card-header">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <a class="navbar-brand" href="https://boomsolutionsve.com/">
                    <img class="w-25" src="{{ asset('assets/img/logocolor.png') }}" alt="Logo de Boom Solutions">
                </a>
                <ul class="navbar-nav">
                    <li class="nav-item active">
                    <a class="nav-link" href="https://boomsolutionsve.com/">Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="https://boomsolutionsve.com/#nosotros">Nosotros</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="https://boomsolutionsve.com/#precios">Precios</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="https://boomsolutionsve.com/#contacto">Contáctanos</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="http://boomsolutions.speedtestcustom.com/">Speedtest</a>
                    </li>
                    <li class="nav-item text-nowrap">
                    <a class="nav-link btn-naranja text-white rounded"
                        href="https://forms.boomsolutionsve.com/facturacion">Registrar pago</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

    <h1 class="font-weight-bold mt-5 text-center" style="color: #001c62;">
        ENCUESTA
    </h1>
    <h5 class="font-weight-bold text-center" style="color: #001c62;">
        Tu opini&oacute;n nos ayuda a crecer, al finalizar esta encuesta podr&aacute;s reportar tu pago
    </h5>

    <div class="card-body w-75 align-self-center mt-5 bg-light">
        @csrf
        <div id="formBasic">
            <form name="BasicForm" id="BasicForm" method="post" action="javascript:void(0)">

                <div class="form-group">
                    <div class="row mt-3">
                        <label for="cedula">C&eacute;dula: </label>
                        <input type="text" class="form-control" id="ced" name="ced" placeholder="C&eacute;dula o Rif - Ej: 12345678 o J-1234567-8">
                        <input type="text" class="form-control" id="mikro" name="mikro" hidden>
                    </div>
                    <div class="row mt-3">    
                        <label for="titular">Titular: </label>
                        <input type="text" class="form-control restricted" id="titular" name="titular" placeholder="Nombre del titular" readonly>
                    </div>
                    <div class="row mt-3">
                        <label for="telefono">Tel&eacute;fono: </label>
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Tel&eacute;fonos" readonly>
                    </div>

                    <div id="advCal">

                    </div>

                    <div class="row mt-3">
                        <div class="col-md-4 mt-4" style="text-align:justify">
                            ¿Con qu&eacute; puntaje calificar&iacute;a usted su servicio de Internet, el &uacute;ltimo mes del 1 al 10? Ten&iacute;endo en cuenta que:
                        </div>
                        <div class="col-md-4">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="opBt1">
                                <label class="form-check-label" for="opBt1">01 - Muy insatisfecho</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="opBt3">
                                <label class="form-check-label" for="opBt3">03 - Deficiente</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="opBt5">
                                <label class="form-check-label" for="opBt5">05 - Regular</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="opBt7">
                                <label class="form-check-label" for="opBt7">07 - Satisfecho</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="opBt9">
                                <label class="form-check-label" for="opBt9">09 - Excelente</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="opBt2">
                                <label class="form-check-label" for="opBt2">02 - Insatisfecho</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="opBt4">
                                <label class="form-check-label" for="opBt4">04 - Irregular</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="opBt6">
                                <label class="form-check-label" for="opBt6">06 - Aceptable</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="opBt8">
                                <label class="form-check-label" for="opBt8">08 - Muy satisfecho</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="opBt10">
                                <label class="form-check-label" for="opBt10">10 - Lo recomiendo</label>
                            </div>
                            </div>
                        </div>
                    </div>

                    <div id="advAtc">

                    </div>

                    <div class="row mt-5">
                        <div class="col-md-3" style="text-align:justify">
                            La atenci&oacute;n que recibes como cliente es: 
                        </div>
                        <div class="col-md-3">
                            <div class="form-check" style="text-align: center">
                                <input class="form-check-input" type="radio" name="flexRadioAtention" id="opBtnAtc1">
                                <label class="form-check-label" for="opBtnAtc1">Excelente</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-check" style="text-align: center">
                                <input class="form-check-input" type="radio" name="flexRadioAtention" id="opBtnAtc2">
                                <label class="form-check-label" for="opBtnAtc2">Buena</label>
                            </div>                            
                        </div>
                        <div class="col-md-3">
                            <div class="form-check" style="text-align: center">
                                <input class="form-check-input" type="radio" name="flexRadioAtention" id="opBtnAtc3">
                                <label class="form-check-label" for="opBtnAtc3">Deficiente</label>
                            </div>                            
                        </div>
                    </div>

                    <h5 class="text-center mt-3" style="color: #001c62;">
                        Coloca el nombre y n&uacute;mero de tel&eacute;fono de cinco (05) seres queridos y te apoyaremos para que tu pr&oacute;xima mensualidad sea
                    </h5>

                    <h4 class="font-weight-bold text-center" style="color: #001c62;">
                        TOTALMENTE GRATIS
                    </h4>

                    <div class="row mt-3">    
                        <div class="col-md-6">
                            <div class="mb-3 row">
                                <label class="col-md-3 col-form-label" style="text-align: center;">Nombre</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="refName01" name="refName01">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3 row">
                                <label class="col-md-3 col-form-label" style="text-align: center;">Tel&eacute;fono: </label>
                                <div class="col-md-9">
                                    <input type="number" class="form-control" id="refTelf01" name="refTelf01">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">    
                        <div class="col-md-6">
                            <div class="mb-3 row">
                                <label class="col-md-3 col-form-label" style="text-align: center;">Nombre</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="refName02" name="refName02">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3 row">
                                <label class="col-md-3 col-form-label" style="text-align: center;">Tel&eacute;fono: </label>
                                <div class="col-md-9">
                                    <input type="number" class="form-control" id="refTelf02" name="refTelf02">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">    
                        <div class="col-md-6">
                            <div class="mb-3 row">
                                <label class="col-md-3 col-form-label" style="text-align: center;">Nombre</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="refName03" name="refName03">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3 row">
                                <label class="col-md-3 col-form-label" style="text-align: center;">Tel&eacute;fono: </label>
                                <div class="col-md-9">
                                    <input type="number" class="form-control" id="refTelf03" name="refTelf03">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">    
                        <div class="col-md-6">
                            <div class="mb-3 row">
                                <label class="col-md-3 col-form-label" style="text-align: center;">Nombre</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="refName04" name="refName04">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3 row">
                                <label class="col-md-3 col-form-label" style="text-align: center;">Tel&eacute;fono: </label>
                                <div class="col-md-9">
                                    <input type="number" class="form-control" id="refTelf04" name="refTelf04">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">    
                        <div class="col-md-6">
                            <div class="mb-3 row">
                                <label class="col-md-3 col-form-label" style="text-align: center;">Nombre</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="refName05" name="refName05">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3 row">
                                <label class="col-md-3 col-form-label" style="text-align: center;">Tel&eacute;fono: </label>
                                <div class="col-md-9">
                                    <input type="number" class="form-control" id="refTelf05" name="refTelf05">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="advBoom">

                    </div>

                    <div class="row mt-3">    
                        <div class="col-md-2">
                            <img src="https://boomsolutionsve.com/wp-content/uploads/2022/05/logo-manitos-boom.png" alt="Manitos Boom" width="75px;">
                        </div>
                        <div class="col-md-4">
                            ¿Conoces nuestra Fundaci&oacute;n Manitos Boom?
                        </div>
                        <div class="col-md-3">
                            <div class="form-check" style="text-align: center;">
                                <input class="form-check-input" type="radio" name="flexRadioMb" id="opBtnMB1">
                                <label class="form-check-label" for="opBtnMB01">Si</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-check" style="text-align: center;">
                                <input class="form-check-input" type="radio" name="flexRadioMb" id="opBtnMB2">
                                <label class="form-check-label" for="opBtnMB02">No</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <button type="submit" class="btn mt-4 btn-naranja btn-lg btn-block" id="siguiente" onclick="NextForms();">Quiero reportar mi pago</button>

                    </div>

                </div>







            </form>
        </div>
    </div>

</div>

@endsection
@section('js')

<script>

let $ced = document.getElementById("ced")

$ced.addEventListener('keydown', () => {
  clearTimeout(timeout)
  timeout = setTimeout(() => {
    $.ajax({
        url: "{{ url('/facturacion/api') }}",
        type: "POST",
        data:   { ced: $('#ced').val() },
        headers:{
            'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 	'application/x-www-form-urlencoded',
        },
        success: function(data, textStatus, xhr)
        {
            if(xhr.status == 200)
            {
                if(data.status == true)
                {
                    $('#titular').val(data.data['nombre']);
                    $('#phone').val(data.data['telefono']);
                    $('#mikro').val(data.data['id']);
                }else{

                    $('#titular').val('');
                    $('#phone').val('');
                    $('#mikro').val('');

                    ErrorMessage('Error', 'Datos incorrectos, por favor verifique nuevamente');
                }
            }
            $.unblockUI();
        }
    });
    clearTimeout(timeout)
  },1500)
})

function restricted(e)
{
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla==8) return true;
    patron =/[A-Za-z\s]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

function NextForms()
{
    let cal = CheckCalficacion();

    if(cal == false)
    {
        $('#advCal').html('<div class="row mt-3 alert alert-info" role="alert"> Debes elegir una opción</div>');
        ErrorMessage('Error', 'Para nosotros su puntaje es importante, por favor elija una opción.');

    }else{
        
        $('#advCal').html('');

        let atc = CheckAtencion();

        if(atc == false)
        {
            $('#advAtc').html('<div class="row mt-3 alert alert-info" role="alert"> Debes elegir una opción</div>');
            ErrorMessage('Error', 'Para nosotros su atención es primordial. Por favor elija una opción.');
        }else{

            $('#advCal').html('');
            $('#advAtc').html('');
            
            let mb  = CheckManitos();
        
            if(mb == false)
            {
                $('#advBoom').html('<div class="row mt-3 alert alert-info" role="alert"> Debes elegir una opción</div>');

                ErrorMessage('Error', 'Debe seleccionar una opcion');
            }else{
                $('#advCal').html('');
                $('#advAtc').html('');
                $('#advBoom').html('');

                if( ($('#ced').val() == '') || ($('#titular').val() == '') || ($('#phone').val() == '') )
                {
                    ErrorMessage('Error', 'Debe ingresar sus datos completos');
                }else{
                    $.ajax({
                    url: "{{ url('/facturacion/api/forms') }}",
                    type: "POST",
                    data:   { forms: $('#BasicForm').serialize(), cal: cal, atc: atc, mb: mb },
                    headers:{
                        'X-CSRF-TOKEN': 	$('meta[name="csrf-token"]').attr('content'),
                        'Content-Type': 	'application/x-www-form-urlencoded',
                    },
                    beforeSend: function(){
                        blockUI();
                    },
                    success: function(data, textStatus, xhr)
                    {
                        if(xhr.status == 200)
                        {
                            $('#BasicForm')[0].reset();
                            SuccessMessage('Gracias', 'Muchas gracias, su información es importante para nosotros, en breves instantes seras redirigido al portal de facturación.');
                            setTimeout(window.location.replace("https://facturacion.boomsolutionsve.com/"), 5000);

                        }
                        $.unblockUI();
                    
                    }
                    });
                }
            }
        }
    }
    
}

function CheckCalficacion()
{
    var term    =   0;
    var tmp     =   0;
    var tcheck  =   $('#opBt').prop('checked');
    for (i=1; i <= 100; i++) 
    { 
        console.log("#opBt"+i);
        if($("#opBt"+i).length > 0) {   if ($("#opBt"+i).is(':checked')) { term = term+1; tmp = i;}  } 
    }

    if( term == 1 ) { return tmp; } return false;
}

function CheckAtencion()
{
    var term    =   0;
    var tmp    =   0;
    var tcheck  =   $('#opBtnAtc').prop('checked');
    for (i=1; i <= 100; i++) 
    { 
        if($("#opBtnAtc"+i).length > 0) {   if ($("#opBtnAtc"+i).is(':checked')) { term = term+1; tmp = i; }  } 
    }

    if( term == 1 ) { return tmp; } return false;
}

function CheckManitos()
{
    var term    =   0;
    var tmp     =   0;
    var tcheck  =   $('#opBtnMB').prop('checked');
    for (i=1; i <= 100; i++) 
    { 
        if($("#opBtnMB"+i).length > 0) {   if ($("#opBtnMB"+i).is(':checked')) { term = term+1; tmp = i;}  } 
    }

    if( term == 1 ) { return tmp; } return false;
}

function ErrorMessage(title, message)
{
    Swal.fire( title, message, 'error');
}

function SuccessMessage(title, message)
{
    Swal.fire( title, message, 'success');
}

function blockUI()
{
    $.blockUI({ css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .5,
        color: '#fff'
    }});
}

</script>


@endsection