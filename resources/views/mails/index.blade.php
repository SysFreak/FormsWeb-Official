@extends('layouts.mail')

@section('content')
<div class="content">
    <table>
        <tr>
            <td>               
                <table class="head-wrap" style="text-align: center;">
                    <tr>
                        <td class="header container">
                            <div class="content">
                                <table>
                                    <tr style="text-align: center;">
                                        <img src="https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png" height="73" width="110">
                                    </tr>
                                    <tr><h4 style="margin-top: 10px;">Datos del Cliente</h4></tr>
                                </table>
                            </div>          
                        </td>
                    </tr>
                </table>

                <br>

                <table class="head-wrap" style="background: #ecf8ff">
                    <tbody>
                        @foreach ($details['data'] as $s => $si)
                        <tr>
                            <td><b>{{ $s }}:</b></td>
                            <td>{{ $si}}</td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>

            </td>
        </tr>
    </table>
</div>

@endsection