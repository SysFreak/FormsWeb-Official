@extends('layouts.email_marketing')

@section('content')

<div style="background-color: #f6f6f6;-webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none;-webkit-text-size-adjust: none;width: 100% !important;height: 100%;line-height: 1.6;margin: 0; padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;">
    <table class="body-wrap" style="background-color: #f6f6f6;width: 100%;">
        <tbody><tr><td>&nbsp;</td><td style="display: block !important; max-width: 600px !important;margin: 0 auto !important;clear: both !important;" width="600">
            <div style="max-width: 600px;margin: 0 auto;display: block;padding: 20px;">
            <table cellpadding="0" cellspacing="0" style=" background: #fff;border: 1px solid #e9e9e9;border-radius: 3px;" width="100%">
                <tbody><tr><td style="vertical-align: middle;font-size: 16px;color: #fff;font-weight: 500;padding: 15px;border-radius: 3px 3px 0 0;border-bottom: 1px solid #e9e9e9;"><img alt="Mostrar Logo" src="https://boomsolutionsve.com/wp-content/uploads/2020/05/logoHeader.png" style="max-height:65px;" />
                    <div style="width: auto;color: #348eda;font-weight: 600;float: right;margin: 10px auto;"><span style="color:#FFFFFF;"><span style="font-size:14px;">Atenci&oacute;n al cliente - Boom Solutions</span></span></div>
                    </td></tr><tr><td style="vertical-align: top;padding: 25px;"><img height="1" src="https://test.boomsolutions.com/email/track/'.{{ $details['track'] }}.'" width="1" />
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tbody><tr><td style="vertical-align: top;padding: 0px;">Saludos,&nbsp;<strong>{{ $details[name] }} </strong></td></tr><tr><td style="vertical-align: top;padding: 0 0 10px;">
                            <p><span style="font-size:12px;"></span></p>

                            <p style="text-align: justify;">Boom Solutions informa que a partir del d&iacute;a jueves 12 de mayo cualquier solicitud relacionada con facturaci&oacute;n o envi&oacute; de retenciones deber&aacute;n ser enviadas al correo electr&oacute;nico:&nbsp;<a href="mailto:servicio.cliente@boomsolutions.com?subject=Servicio%20al%20Cliente" target="_blank">servicio.cliente@boomsolutions.com</a></p>

                            <p style="text-align: justify;">Recuerda que puede reportar el pago a trav&eacute;s de nuestra&nbsp;Portal Web:&nbsp;<a data-saferedirecturl="https://www.google.com/url?q=https://facturacion.boomsolutionsve.com/cliente/login&amp;source=gmail&amp;ust=1652449672244000&amp;usg=AOvVaw1Tm56MycqxIspq8Wlx0lM_" href="https://facturacion.boomsolutionsve.com/cliente/login" target="_blank">https://facturacion.<wbr />boomsolutionsve.com/cliente/<wbr />login</a></p>

                            <p style="text-align: center;"><a href="https://boomsolutionsve.com/" target="_blank"><img alt="Boom Solutions" src="https://boomsolutionsve.com/wp-content/uploads/2022/05/LOGO_BOOM_SOLUTIONS.png" style="height: 124px; width: 250px;" /></a></p>
                            </td></tr><tr><td style="vertical-align: top;padding: 0 0 20px;">
                            <p style="text-align: center;"><strong>Gracias por preferirnos</strong></p>
                            </td></tr>
                        </tbody>
                    </table>
                    </td></tr>
                </tbody>
            </table>

            <div style="width: 100%;clear: both;color: #999;padding: 20px;">
            <table width="100%">
                <tbody><tr><td class="aligncenter" style="vertical-align: top; padding: 0px 0px 20px; font-size: 12px;">.</td></tr>
                </tbody>
            </table>
            </div>
            </div>
            </td><td>&nbsp;</td></tr>
        </tbody>
    </table>
</div>
@endsection
