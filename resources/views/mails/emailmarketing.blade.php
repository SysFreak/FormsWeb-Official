@extends('layouts.email_marketing')

@section('content')

<div style="background-color: #f6f6f6;-webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none;-webkit-text-size-adjust: none;width: 100% !important;height: 100%;line-height: 1.6;margin: 0; padding: 0;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;">
    <table class="body-wrap" style="background-color: #f6f6f6;width: 100%;">
        <tbody><tr><td>&nbsp;</td><td style="display: block !important; max-width: 600px !important;margin: 0 auto !important;clear: both !important;" width="600">
            <div style="max-width: 600px;margin: 0 auto;display: block;padding: 20px;">
            <table cellpadding="0" cellspacing="0" style=" background: #fff;border: 1px solid #e9e9e9;border-radius: 3px;" width="100%">
                <tbody><tr><td style="vertical-align: middle;font-size: 16px;color: #fff;font-weight: 500;padding: 15px;border-radius: 3px 3px 0 0;border-bottom: 1px solid #e9e9e9;"><img alt="Mostrar Logo" src="https://boomsolutionsve.com/wp-content/uploads/2020/05/logoHeader.png" style="max-height:65px;" />
                    <div style="width: auto;color: #348eda;font-weight: 600;float: right;margin: 10px auto;"><span style="color:#FFFFFF;"><span style="font-size:14px;">Atenci&oacute;n al cliente - Boom Solutions</span></span></div>
                    </td></tr><tr><td style="vertical-align: top;padding: 25px;">
                    <table cellpadding="0" cellspacing="0" width="100%">
                            <img src="https://test.boomsolutions.com/email/track/'.{{ $details['track'] }}.'" width="1" height="1" />
                            <tbody><tr><td style="vertical-align: top;padding: 0px;">Saludos,&nbsp;<strong>{{ $details['name'] }} </strong></td></tr><tr><td style="vertical-align: top;padding: 0 0 10px;">
                            <p><span style="font-size:12px;"></span></p>

                            {{-- <p>{{ $details['web'] }}email/track/{{ $details['track'] }}</p> --}}

                            <p>Premiamos tu fidelidad y por eso al <strong>REPORTAR TU PAGO</strong>&nbsp;del mes de <strong>MAYO</strong>&nbsp;<b>antes del 30 de ABRIL</b>, participas por una mensualidad libre de costo, indiferentemente de tu plan.</p>

                            <table border="0" cellpadding="0" cellspacing="0" style="width:500px;">
                                <tbody><tr><td><b>Reporta tu pago en:&nbsp;</b></td></tr><tr><td>Portal Web:&nbsp;<a data-saferedirecturl="https://www.google.com/url?q=https://facturacion.boomsolutionsve.com/cliente/login&amp;source=gmail&amp;ust=1650554517832000&amp;usg=AOvVaw3Mop0eWkPapE1JEugMURb3" href="https://facturacion.boomsolutionsve.com/cliente/login" target="_blank">https://facturacion.<wbr />boomsolutionsve.com/cliente/<wbr />login</a></td></tr>
                                </tbody>
                            </table>
                            &nbsp;

                            <table border="0" cellpadding="0" cellspacing="0" style="width:500px;">
                                <tbody><tr><td><b>Tel&eacute;fonos de contacto para reportar tu pago:</b></td></tr><tr><td>V&iacute;a llamada: <a href="tel:+582513353330">02513353330</a></td></tr><tr><td>V&iacute;a WhatsApp:&nbsp;<a data-saferedirecturl="https://www.google.com/url?q=https://wa.me/584121561895&amp;source=gmail&amp;ust=1650554517832000&amp;usg=AOvVaw3DVW6G_cpTt-JMMfC_m5gn" href="https://wa.me/584121561895" target="_blank">https://wa.me/<wbr />584121561895</a></td></tr>
                                </tbody>
                            </table>

                            <p style="text-align: justify;">&iexcl;Reporta y participa ya!</p>

                            <p style="text-align: justify;"><a href="https://boomsolutionsve.com/" target="_blank"><img alt="Boom Solutions" src="https://boomsolutionsve.com/wp-content/uploads/2022/04/marketing.png" style="width: 600px; height: 750px;" /></a></p>
                            </td></tr><tr><td style="vertical-align: top;padding: 0 0 20px;">
                            <p style="text-align: center;"><strong>Gracias por preferirnos</strong></p>
                            </td></tr>
                        </tbody>
                    </table>
                    </td></tr>
                </tbody>
            </table>

            <div style="width: 100%;clear: both;color: #999;padding: 20px;">
            <table width="100%">
                <tbody><tr><td class="aligncenter" style="vertical-align: top; padding: 0px 0px 20px; font-size: 12px;">.</td></tr>
                </tbody>
            </table>
            </div>
            </div>
            </td><td>&nbsp;</td></tr>
        </tbody>
    </table>
</div>

@endsection
